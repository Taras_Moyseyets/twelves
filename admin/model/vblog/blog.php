<?php
class ModelVblogBlog extends Model {
    public function getInfoPost($id,$size_img = false){
        $query = $this->db->query("SELECT * FROM vblog WHERE id = ".$id." limit 1");
        $this->load->model('tool/image');
        $query->row['image_small'] = $this->model_tool_image->resize($query->row['image'], ($size_img[0]?$size_img[0]:100), ($size_img[1]?$size_img[1]:100));
        return $query->row;
    }
    
    public function saveData($id,$data){
        //print_r($data);
        $query = $this->db->query("UPDATE vblog SET title='".addslashes($data['title'])."',text='".addslashes($data['text'])."',image=\"".$data['image']."\" WHERE id = ".$id);
        /*echo "UPDATE vblog SET title='".addslashes($data['title'])."',text='".$data['text']."',image=\"".$data['image']."\" WHERE id = ".$id;
        exit;*/
        return true;
    }
    
    public function addPost($data){
        $query = $this->db->query("INSERT INTO vblog (title,text,time,image) VALUES (\"".addslashes($data['title'])."\",\"".$data['text']."\",\"".time()."\",\"".$data['image']."\")");
        return true;
    }
	
    public function deletePost($id){
        $this->db->query("DELETE  FROM vblog WHERE id='".$id."'");

	}

    public function checkDB(){
    $query = $this->db->query("show tables like 'vblog'");

    if(!$query->row)
        $this->db->query("
            CREATE TABLE `vblog` (
            	`id` INT(11) NOT NULL AUTO_INCREMENT,
            	`title` VARCHAR(250) NULL DEFAULT NULL,
            	`text` TEXT NULL,
            	`image` VARCHAR(128) NULL DEFAULT NULL,
            	`time` VARCHAR(128) NULL DEFAULT NULL,
            	PRIMARY KEY (`id`)
            )");
    }

    public function viewAllPosts($size_img = false){
  		$query = $this->db->query("SELECT * FROM vblog ORDER BY time DESC");
        $this->load->model('tool/image');
        
		foreach ($query->rows as $result) {
            $result['time'] = date('m/d',$result['time']);
            $result['image'] = $this->model_tool_image->resize($result['image'], ($size_img[0]?$size_img[0]:100), ($size_img[1]?$size_img[1]:100));
            $result['text'] = html_entity_decode($result['text']);
            $result['title'] = htmlspecialchars_decode(trim(html_entity_decode($result['title'])));
            $result['edit_link'] = $this->url->link('vblog/post/edit', array('id_post'=>$result['id'],'token'=>$this->session->data['token']), 'SSL');
            $result['drop_link'] = $this->url->link('vblog/blog/drop', array('id_post'=>$result['id'],'token'=>$this->session->data['token']), 'SSL');
            $posts[] = $result;
		}
        if(isset($posts))
            return $posts;
        else
            return array();
    }
    
    public function cut_text($text,$maxchar=100){
        $text = $this->del_html($text);
        $words = explode(' ',trim($text));
        $text="";
        foreach ($words as $word) {
			if (mb_strlen($text.' '.$word)<$maxchar) {
				$text.=' '.$word;
			}
			else {
				$text.='...';
				break;
			}
		}
        return $text;
    }
    
    public function del_html($text){
        return trim(preg_replace('/\\<\/?.*\>/U', '', htmlspecialchars_decode(trim(html_entity_decode($text)))));
    }
}
?>