<?php
class ControllerVblogPost extends Controller {
	private $error = array();
    public $type;
    
    public function edit(){
        $this->load->model('vblog/blog');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $POST = $this->request->post;
            $this->model_vblog_blog->saveData($this->request->get['id_post'],$POST);
		}
        $this->index();
    }
    
    public function add(){
        $this->load->model('vblog/blog');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $POST = $this->request->post;
            $this->model_vblog_blog->addPost($POST);
		}
        $this->index();
    }
    
    public function loadDataPost(){
        $this->load->model('vblog/blog');
        return $this->model_vblog_blog->getInfoPost($this->request->get['id_post']);
    }

	public function index() {
        $this->load->language('vblog/blog'); //подключаем наш языковой файл
        $arr_route = explode('/',$this->request->get['route']);
        if(count($arr_route)===3)
            $this->type = $arr_route[2];
        else
            return false;
        
        if($this->type=='edit')
            $data['form']=$this->loadDataPost();
        else{
            $data['form']['title'] = '';
            $data['form']['text'] = '';
        }
        
        if(empty($data['form']['image'])){
            $this->load->model('tool/image');
            $data['form']['image'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }
        
		$data['heading_title'] = $this->language->get('heading_title');
        $data['text_edit'] = $this->language->get('text_edit');

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('vblog/blog', 'token=' . $this->session->data['token'], 'SSL')
		);

        // если метод validate вернул warning, передадим его представлению
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

        //ссылки для формы и кнопки "cancel"
		$data['action'] = $this->url->link('vblog/post/'.$this->type, ($this->type=='edit'?'id_post='.$this->request->get['id_post']:'').'&token='.$this->session->data['token'], 'SSL');
		$data['cancel'] = $this->url->link('extension/vblog', 'token=' . $this->session->data['token'], 'SSL');

		//переменная с статусом модуля
        if (isset($this->request->post['mymodul_status'])) {
			$data['mymodul_status'] = $this->request->post['mymodul_status'];
		} else {
			$data['mymodul_status'] = $this->config->get('mymodul_status');
		}

        //ссылки на контроллеры header,column_left,footer, иначе мы не сможем вывести заголовок, подвал и левое меню в файле представления
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

        //в качестве файла представления модуля для панели администратора использовать файл mymodul.tpl
		$this->response->setOutput($this->load->view('vblog/post.tpl', $data));
	}

    //обязательный метод в контроллере, он запускается для проверки разрешено ли пользователю изменять настройки данного модуля
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'vblog/blog')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
}
?>