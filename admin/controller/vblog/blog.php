<?php
class ControllerVblogBlog extends Controller {
	private $error = array();

    public function index() {
        $this->load->language('vblog/blog'); //подключаем наш языковой файл
        $this->load->model('vblog/blog');
        //створюємо табличку в БД як треба
        $this->model_vblog_blog->checkDB();
        // ваши переменные
        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_edit'] = $this->language->get('text_edit');
        
        // далее идет формирование массива breadcrumbs (хлебные крошки)
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
        	'text' => $this->language->get('text_home'),
        	'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
        	'text' => $this->language->get('heading_title'),
        	'href' => $this->url->link('vblog/blog', 'token=' . $this->session->data['token'], 'SSL')
        );
        
        $data['add_link'] = $this->url->link('vblog/post/add', 'token='.$this->session->data['token'], 'SSL');
        
        $data['posts'] = $this->model_vblog_blog->viewAllPosts();
        foreach($data['posts'] as $key => $post)
            $data['posts'][$key]['title'] = $this->model_vblog_blog->del_html($post['title']);

        // если метод validate вернул warning, передадим его представлению
        if (isset($this->error['warning'])) {
        	$data['error_warning'] = $this->error['warning'];
        } else {
        	$data['error_warning'] = '';
        }
        
        //ссылки на контроллеры header,column_left,footer, иначе мы не сможем вывести заголовок, подвал и левое меню в файле представления
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        
        //в качестве файла представления модуля для панели администратора использовать файл mymodul.tpl
        $this->response->setOutput($this->load->view('vblog/blog.tpl', $data));
    }
	   public function drop() {
		     $this->load->model('vblog/blog');
			$this->model_vblog_blog->deletePost($this->request->get['id_post']);
			$this->response->redirect($this->url->link('vblog/blog', 'token=' . $this->session->data['token']));
	   
	   }
    //обязательный метод в контроллере, он запускается для проверки разрешено ли пользователю изменять настройки данного модуля
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'vblog/blog')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
}
?>