<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row window-forgot-your-password"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1 class="question-register h1-forgot-password">Ваш аккаунт создан!</h1>
      <div class="success-registration-p">
        <p>Поздравляем! Ваш новый аккаунт был успешно создан!</p>
        <p>Теперь вы можете воспользоваться привилегиями для повышения вашего опыта покупок в Интернете с нами.</p>
        <p>Если у вас есть вопросы по поводу работы этого интернет-магазина, пожалуйста, свяжитесь по электронной почте с владельцем магазина.</p>
        <p>Подтверждение регистрации было отправлено на указанный электронный адрес. Если вы не получили его в течение часа, пожалуйста, свяжитесь с нами.</p>
      </div>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="send-button">Продолжить</a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

			<?php if (isset($script_order)) { echo $script_order; } ?>
			<?php echo $footer; ?>
			