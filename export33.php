<?php
ini_set('display_errors',1);
ini_set('error_reporting',2047);
require_once 'PHPExcel/IOFactory.php';
require_once 'config.php';
global $cat;
$objPHPExcel = PHPExcel_IOFactory::load("export.xlsx");
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
if (!$conn) {
    die("Connection failed: " . $conn->connect_error);
}
$conn->set_charset('utf-8');
mysqli_query($conn, "SET NAMES 'utf-8'");
mysqli_query($conn, "SET CHARACTER SET 'utf8';");
mysqli_query($conn, "SET SESSION collation_connection = 'utf8_general_ci';");
function check_manufac($conn,$name){
    $query_manufacturer = "SELECT manufacturer_id AS id FROM oc_manufacturer WHERE name = '".$name."'";
    $manufacturer = mysqli_query($conn, $query_manufacturer);
        
        
        $manufacturer = $manufacturer->fetch_assoc();
        
    if (isset($manufacturer['id'])) {
        $manufacturer_id = $manufacturer['id'];
        //while($row = $manufacturer->fetch_row()) {
        //    $rows[]=$row;
        //}
        //for ($i=0; $i < count($rows); $i++) {
        //    if ($name == $rows['name']) {
        //        $manufacturer_id = $rows['manufacturer_id'];
    }
    else{
        $query = "INSERT INTO oc_manufacturer(name) VALUES('".$name."')";
        if ($conn->query($query) === TRUE) {
            $manufacturer_id = $conn->insert_id;
        }       
    }
    
    return $manufacturer_id;
       
//select
//else
//insert
}

function check_type($conn,$type){
    $query_type = "SELECT attribute_group_id AS id FROM oc_attribute_group_description WHERE name = '".$type."'"; 
    $type_query = mysqli_query($conn, $query_type);     
        $type_query= $type_query->fetch_assoc();        
    if (isset($type_query['id'])) {  
        $type_id = $type_query['id'];
    }
    else{
        $query = "INSERT INTO oc_attribute_group(sort_order) VALUES('0')";           
        if ($conn->query($query) === TRUE) {
            $type_id = $conn->insert_id;
        }
        $query2 = "INSERT INTO oc_attribute_group_description(attribute_group_id, language_id, name) VALUES('".$type_id."','1' ,'".$type."')";
        //echo $query2;
        mysqli_query($conn, $query2);

    }    
    return $type_id;
}
function check_type_desc($conn,$type, $id){
    $query_type = "SELECT attribute_id AS id FROM oc_attribute_description WHERE name = '".$type."'";
    $type_query = mysqli_query($conn, $query_type);
    $type_query = $type_query->fetch_assoc();        
    if (isset($type_query['id'])) {
        $type_id = $type_query['id'];
    }
    else{
        $query = "INSERT INTO oc_attribute(attribute_group_id) VALUES('".$id."')";
        
        if ($conn->query($query) === TRUE) {
            $type_id = $conn->insert_id;
        }
        $query_ = "INSERT INTO oc_attribute_description(name, language_id,attribute_id) VALUES('".$type."','1','".$type_id."')";
		echo $query_;
        mysqli_query($conn, $query_);
              
    }    
    return $type_id;
}
function filter_check_type($conn,$type){
    $query_type = "SELECT filter_group_id AS id FROM oc_filter_group_description WHERE name = '".$type."'"; 
    $type_query = mysqli_query($conn, $query_type);     
        $type_query= $type_query->fetch_assoc();        
    if (isset($type_query['id'])) {  
        $type_id = $type_query['id'];
    }
    else{
        $query = "INSERT INTO oc_filter_group(sort_order) VALUES('0')";           
        if ($conn->query($query) === TRUE) {
            $type_id = $conn->insert_id;
        }
        $query2 = "INSERT INTO oc_filter_group_description(filter_group_id, language_id, name) VALUES('".$type_id."','1' ,'".$type."')";
        //echo $query2;
        mysqli_query($conn, $query2);
    }    

    return $type_id;
}

function filter_to_cat($conn,$type,$filter_id,$category){
    echo "FILTER:".$filter_id."<BR>";
    $category_id=to_category($conn,$category,0);
    $query_type = "SELECT filter_id AS id FROM oc_category_filter WHERE filter_id = '".$filter_id."'"; 
    $type_query = mysqli_query($conn, $query_type);     
        $type_query= $type_query->fetch_assoc();        
    if (isset($type_query['id'])) {  
        $type_id = $type_query['id'];
    }
    else{
        $query = "INSERT INTO oc_category_filter(category_id,filter_id) VALUES('$category_id','$filter_id')";  
        echo $query;
        if ($conn->query($query) === TRUE) {
            $type_id = $conn->insert_id;
        }
       // $query2 = "INSERT INTO oc_filter_group_description(filter_group_id, language_id, name) VALUES('".$type_id."','1' ,'".$type."')";
        //echo $query2;
        //mysqli_query($conn, $query2);
    }    
    //return $type_id;
}

function filter_check_type_desc($cat,$conn,$type, $id){ 
    $query_type = "SELECT filter_id AS id FROM oc_filter_description WHERE name = '".$type."'";
    $type_query = mysqli_query($conn, $query_type);
    $type_query = $type_query->fetch_assoc();
    
    if (isset($type_query['id'])) {
        $type_id = $type_query['id'];
    }
    else{
        $query = "INSERT INTO oc_filter(filter_group_id) VALUES('".$id."')";
        if ($conn->query($query) === TRUE) {
            $type_id = $conn->insert_id;
        }
        $query = "INSERT INTO oc_filter_description(filter_id, filter_group_id, name, language_id) VALUES('".$type_id."','".$id."','".$type."','1')";
        mysqli_query($conn, $query);
    }    
    filter_to_cat($conn,$type,$type_id,$cat);
    return $type_id;
}
function to_category($conn, $type, $id){
    if ($type == 'наручные') {
        $query_type = "SELECT category_id AS id FROM oc_category_description WHERE name = 'НАРУЧНЫЕ ЧАСЫ'";
        $type_query = mysqli_query($conn, $query_type);
        $type_query = $type_query->fetch_assoc();        
        $type_id = $type_query['id'];
    }
    elseif ($type == 'настенные') {
        $query_type2 = "SELECT category_id AS id FROM oc_category_description WHERE name = 'НАСТЕННЫЕ ЧАСЫ'";
        $type_query2 = mysqli_query($conn, $query_type2);
        $type_query2 = $type_query2->fetch_assoc();        
        $type_id = $type_query2['id'];
    }
    else {
        $query_type3 = "SELECT category_id AS id FROM oc_category_description WHERE name = 'АКСЕССУАРЫ'";
        $type_query3 = mysqli_query($conn, $query_type3);
        $type_query3 = $type_query3->fetch_assoc();        
        $type_id = $type_query3['id'];
    }
        //$query = "INSERT INTO (product_id, category_id) VALUES('".$id."','".$type_query."')";
        //mysqli_query($conn, $query);
        //$type_query1 = $type_query1->fetch_assoc();
        //$type_id = $type_query1['id'];  
   
    return $type_id;
}

foreach ($objPHPExcel->getWorksheetIterator() as $key=>$worksheet) {

    //echo $key;
    $worksheetTitle     = $worksheet->getTitle();
    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
    $nrColumns = ord($highestColumn) - 64;
    echo "<br>The worksheet ".$worksheetTitle." has ";
    echo $nrColumns . ' columns (A-' . $highestColumn . ') ';
    echo ' and ' . $highestRow . ' row.';
    echo '<br>Data: <table border="1"><tr>';

    echo $highestColumnIndex;
    $column_arr = array('','manufacturer','collection','name','sku','price','size','type','sex','mechanism','material','color','glass','country','length','style','water','desc','photo','photo1','photo2','photo3','photo4','photo5','photo6','photo7','photo8');
    
    $type_id = check_type($conn,'Тип');
    $sex_id = check_type($conn,'Пол');
    $mech_id = check_type($conn,'Механизм');
    $mater_id = check_type($conn,'Материал');
    $color_id = check_type($conn,'Цвет');
    $glass_id = check_type($conn,'Стекло');
    $style_id = check_type($conn,'Стиль');
    $country_id = check_type($conn,'Страна');
    $length_id = check_type($conn,'Длина ремешка');
    $water_id = check_type($conn,'Водонепроницаемость');

    $filter_type_id = filter_check_type($conn,'Тип');
    $filter_sex_id = filter_check_type($conn,'Пол');
    $filter_mech_id = filter_check_type($conn,'Механизм');
    $filter_mater_id = filter_check_type($conn,'Материал');
    $filter_color_id = filter_check_type($conn,'Цвет');
    $filter_glass_id = filter_check_type($conn,'Стекло');
    $filter_style_id = filter_check_type($conn,'Стиль');
    $filter_country_id = filter_check_type($conn,'Страна');
	$i=0;
    for ($row = 4; $row <= $highestRow-8; ++ $row) { //$highestRow-8
	
	//if($i==6)break;
	$i++;
        echo '<tr>';
        $desc = '';
        for ($col = 1; $col < $highestColumnIndex-1; ++ $col){
            $cell = $worksheet->getCellByColumnAndRow($col, $row);
            $val = $cell->getValue();

            $type = $column_arr[$col];
            if($type == 'type'){
				if(empty(trim($type)))continue;
               // $type_id = check_type($conn,'type');
                $type_des_id = check_type_desc($conn,"Тип",$type_id);
                $categor = $val;
       
                //echo $type_id;
                //echo $type_des_id;
                //$query_type = "INSERT INTO oc_attribute(attribute_group_id) VALUES('".$type_id."')";
                //mysqli_query($conn, $query_type);
                //$query_type_des = "INSERT INTO oc_attribute(attribute_id) VALUES('".$type_des_id."')";
               // mysqli_query($conn, $query_type_des);
                //$query_type_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$type_des_id."','".$type_des_id."', '1', 'type'";
                //mysqli_query($conn, $query_type_prod_des);
                //$categ = to_category($conn,$val,$type_id);
                //$query_to_cat = "INSERT INTO (product_id, category_id) VALUES('".$type_id."','".$categ."')";
                //mysqli_query($conn, $query_to_cat);

                $filter_type_des_id = filter_check_type_desc($categor,$conn, $val, $filter_type_id);
                //$filter_query_type_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$filter_type_des_id."','".$filter_type_des_id."')";
                //mysqli_query($conn, $filter_query_type_prod_des);
            }
            if($type == 'manufacturer'){
                $id_man = check_manufac($conn,$val);
            }
            if($type == 'collection'){
                $collection = $val;
            }
            if($type == 'name'){
                $name = $val;
            }
            if($type == 'sku'){
                $sku = $val;
            }
            if($type == 'price'){
               $price = $val;
                //$query_price = "INSERT INTO oc_product(price) VALUES('".$price."')";
                //mysqli_query($conn, $query_price);
            }
            if($type == 'size'){
                $size = $val;
                if (isset($size)) {
                    $size_ = explode(" ", $size);                
                }
                else {
                    $size_[0] = ' ';
                    $size_[2] = ' ';
                }                
            }
            if($type == 'country'){
                $country_des_id = check_type_desc($conn,"Страна",$country_id);
                $filter_country_des_id = filter_check_type_desc($categor,$conn, $val, $filter_country_id);
				$country = $val;
            }
            if($type == 'length'){
                $length_des_id = check_type_desc($conn,"Длина ремешка",$length_id);
				$length=$val;
            }
            if($type == 'desc'){
                $desc = $val;
            }
            if($type == 'water'){
                $water_des_id = check_type_desc($conn,"Водонепроницаемость",$water_id);
				$water=$val;
            }    

            
            if($type == 'sex'){
               // $sex_id = check_type($conn,'sex');
                $sex_des_id = check_type_desc($conn,"Пол",$sex_id);
				$sex=$val;
                //echo $type_id;
                //echo $type_des_id;
                //$query_sex = "INSERT INTO oc_attribute(attribute_group_id) VALUES('".$sex_id."')";
                //mysqli_query($conn, $query_sex);
               // $query_sex_des = "INSERT INTO oc_attribute(attribute_id) VALUES('".$sex_des_id."')";
               // mysqli_query($conn, $query_sex_des);
                //$query_sex_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$sex_des_id."','".$sex_des_id."', '1', 'sex')";
                //mysqli_query($conn, $query_sex_prod_des);
                $filter_sex_des_id = filter_check_type_desc($categor,$conn,$val,$filter_sex_id);
                //$filter_query_sex_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$filter_sex_des_id."','".$filter_sex_des_id."')";
                //mysqli_query($conn, $filter_query_sex_prod_des);
            }
            if($type == 'mechanism'){
                $mech_des_id = check_type_desc($conn,"Механизм",$mech_id);
				$mech=$val;
                //$query_mech = "INSERT INTO oc_attribute(attribute_group_id) VALUES('".$mech_id."')";
                //mysqli_query($conn, $query_mech);
               // $query_mech_des = "INSERT INTO oc_attribute(attribute_id) VALUES('".$mech_des_id."')";
                //mysqli_query($conn, $query_mech_des);
                //$query_mech_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$mech_des_id."','".$mech_des_id."', '1', 'mech')";
                //mysqli_query($conn, $query_mech_prod_des);
                $filter_mech_des_id = filter_check_type_desc($categor,$conn,$val,$filter_mech_id);
                //$filter_query_mech_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$filter_mech_des_id."','".$filter_mech_des_id."')";
                //mysqli_query($conn, $filter_query_mech_prod_des);
            }
            if($type == 'material'){
                $mater_des_id = check_type_desc($conn,"Материал",$mater_id);
				$mater=$val;
                //$query_mater_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$mater_des_id."','".$mater_des_id."', '1', 'mater')";
                //mysqli_query($conn, $query_mater_prod_des);

                $filter_mater_des_id = filter_check_type_desc($categor,$conn,$val,$filter_mater_id);
                //$filter_query_mater_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$filter_mater_des_id."','".$filter_mater_des_id."')";
                //mysqli_query($conn, $filter_query_mater_prod_des);
            }
            if($type == 'color'){
                $color_des_id = check_type_desc($conn,"Цвет",$color_id);
				$color=$val;
                //$query_color_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$color_des_id."','".$color_des_id."', '1', 'color')";
                //mysqli_query($conn, $query_color_prod_des);

                $filter_color_des_id = filter_check_type_desc($categor,$conn,$val,$filter_color_id);
                //$filter_query_color_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$filter_color_des_id."','".$filter_color_des_id."')";
                //mysqli_query($conn, $filter_query_color_prod_des);
            }
            if($type == 'glass'){
                $glass_des_id = check_type_desc($conn,"Стекло",$glass_id);
				$glass=$val;
                //$query_glass_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$glass_des_id."','".$glass_des_id."', '1', 'glass')";
                //mysqli_query($conn, $query_glass_prod_des);

                $filter_glass_des_id = filter_check_type_desc($categor,$conn,$val,$filter_glass_id);
                //$filter_query_glass_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$filter_glass_des_id."','".$filter_glass_des_id."')";
                //mysqli_query($conn, $filter_query_glass_prod_des);                 
            }
            if($type == 'style'){
				if(empty($type))continue;
                $style_des_id = check_type_desc($conn,"Стиль",$style_id);
				$style=$val;
                $filter_style_des_id = filter_check_type_desc($categor,$conn,$val,$filter_style_id);                
            }
            if($type == 'photo'){
                $photo = $val;
            }
            if($type == 'photo1'){
                $photo1 = $val;
            }
            if($type == 'photo2'){
                $photo2 = $val;
            }
            if($type == 'photo3'){
                $photo3 = $val;
            }
            if($type == 'photo4'){
                $photo4 = $val;
            }
            if($type == 'photo5'){
                $photo5 = $val;
            }
            if($type == 'photo6'){
                $photo6 = $val;
            }
            if($type == 'photo7'){
                $photo7 = $val;
            }
            if($type == 'photo8'){
                $photo8 = $val;
            }
        }
                $parent_prod = "SELECT product_id AS id FROM oc_product WHERE model = '".$collection."'"; 
                $parent_query = mysqli_query($conn, $parent_prod);
                $parent_query = $parent_query->fetch_assoc();
                if (isset($parent_query['id'])) {
                    $parent_id = $parent_query['id'];
                    $query_parent = "INSERT INTO oc_product_parent(product_id, product_sku) VALUES('".$parent_id."','".$sku."')";
                mysqli_query($conn, $query_parent);
                }


                $query_product = "INSERT INTO oc_product(manufacturer_id, model, sku, price, length, width,status,quantity) VALUES('".$id_man."','".$collection."','".$sku."','".$price."','".$size_[0]."','".$size_[2]."',1,1)";
                if ($conn->query($query_product) === TRUE) {
                $product_id = $conn->insert_id;
                }
       //oc_product_tostore
	    $query = "INSERT INTO oc_product_to_store(product_id, store_id) VALUES('".$product_id."', '0')";
        mysqli_query($conn, $query);
		$query = "INSERT INTO oc_product_to_layout(product_id, store_id,layout_id) VALUES('".$product_id."', '0',0)";
        mysqli_query($conn, $query);


	   //type
	
			  
              
                $query_type_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$product_id."','".$type_des_id."', '1', '".$categor."')";
                mysqli_query($conn, $query_type_prod_des);
                $categ = to_category($conn,$categor,$type_id);
                $query_to_cat = "INSERT INTO oc_product_to_category(product_id, category_id) VALUES('".$product_id."','".$categ."')";
                mysqli_query($conn, $query_to_cat);

                //$filter_type_des_id = filter_check_type_desc($conn, $val, $filter_type_id);
                $filter_query_type_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$product_id."','".$filter_type_des_id."')";
                mysqli_query($conn, $filter_query_type_prod_des);
        //sex
                $query_sex_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$product_id."','".$sex_des_id."', '1', '".$sex."')";
                mysqli_query($conn, $query_sex_prod_des);
                //$filter_sex_des_id = filter_check_type_desc($conn,$val,$filter_sex_id);
                $filter_query_sex_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$product_id."','".$filter_sex_des_id."')";
                mysqli_query($conn, $filter_query_sex_prod_des);
        //mechanism
                $query_mech_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$product_id."','".$mech_des_id."', '1', '".$mech."')";
                mysqli_query($conn, $query_mech_prod_des);
                //$filter_mech_des_id = filter_check_type_desc($conn,$val,$filter_mech_id);
                $filter_query_mech_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$product_id."','".$filter_mech_des_id."')";
                mysqli_query($conn, $filter_query_mech_prod_des);
        //material
                $query_mater_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$product_id."','".$mater_des_id."', '1', '".$mater."')";
                mysqli_query($conn, $query_mater_prod_des);
                //$filter_mater_des_id = filter_check_type_desc($conn,$val,$filter_mater_id);
                $filter_query_mater_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$product_id."','".$filter_mater_des_id."')";
                mysqli_query($conn, $filter_query_mater_prod_des);
        //color
                $query_color_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$product_id."','".$color_des_id."', '1', '".$color."')";
                mysqli_query($conn, $query_color_prod_des);
                //$filter_color_des_id = filter_check_type_desc($conn,$val,$filter_color_id);
                $filter_query_color_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$product_id."','".$filter_color_des_id."')";
                mysqli_query($conn, $filter_query_color_prod_des);    
        //glass
                $query_glass_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$product_id."','".$glass_des_id."', '1', '".$glass."')";
                mysqli_query($conn, $query_glass_prod_des);

                //$filter_glass_des_id = filter_check_type_desc($conn,$val,$filter_glass_id);
                $filter_query_glass_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$product_id."','".$filter_glass_des_id."')";
                mysqli_query($conn, $filter_query_glass_prod_des);
        //style
			if(!empty($style)){
                $query_style_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$product_id."','".$style_des_id."', '1', '".$style."')";
                mysqli_query($conn, $query_style_prod_des);
                //$filter_glass_des_id = filter_check_type_desc($conn,$val,$filter_glass_id);
                $filter_query_style_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$product_id."','".$filter_style_des_id."')";
				mysqli_query($conn, $filter_query_style_prod_des);
			}
        //country
                $query_country_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$product_id."','".$country_des_id."', '1', '".$country."')";
                mysqli_query($conn, $query_country_prod_des);
                //$filter_glass_des_id = filter_check_type_desc($conn,$val,$filter_glass_id);*/
                $filter_query_country_prod_des = "INSERT INTO oc_product_filter(product_id, filter_id) VALUES('".$product_id."','".$filter_country_des_id."')";
                mysqli_query($conn, $filter_query_country_prod_des);
        //desc
                $description_temp = $collection.$name;
                $query_descrip = "INSERT INTO oc_product_description(product_id, name, language_id, description) VALUES('".$product_id."','".$description_temp."', '1', '".$desc."')";
                mysqli_query($conn, $query_descrip);
        //length
                $query_length_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$product_id."','".$length_des_id."', '1', '".$length."')";
                mysqli_query($conn, $query_length_prod_des);
        //water
                $query_water_prod_des = "INSERT INTO oc_product_attribute(product_id, attribute_id, language_id, text) VALUES('".$product_id."','".$water_des_id."', '1', '".$water."')";
                mysqli_query($conn, $query_water_prod_des);
        //photo 
                if($photo != ''){
                $query_photo = "UPDATE oc_product SET image = 'catalog/demo/".$photo.".jpg' WHERE product_id = '".$product_id."'";
                mysqli_query($conn, $query_photo);
                }
                if($photo1 != ''){
                $query_photo1 = "INSERT INTO oc_product_image(product_id, image) VALUES('".$product_id."','catalog/demo/".$photo1.".jpg')";
                mysqli_query($conn, $query_photo1);
                }
                if($photo2 != ''){
                $query_photo2 = "INSERT INTO oc_product_image(product_id, image) VALUES('".$product_id."','catalog/demo/".$photo2.".jpg')";
                mysqli_query($conn, $query_photo2);
                }
                if($photo3 != ''){
                $query_photo3 = "INSERT INTO oc_product_image(product_id, image) VALUES('".$product_id."','catalog/demo/".$photo3.".jpg')";
                mysqli_query($conn, $query_photo3);
                }
                if($photo4 != ''){
                $query_photo4 = "INSERT INTO oc_product_image(product_id, image) VALUES('".$product_id."','catalog/demo/".$photo4.".jpg')";
                mysqli_query($conn, $query_photo4);
                }
                if($photo5 != ''){
                $query_photo5 = "INSERT INTO oc_product_image(product_id, image) VALUES('".$product_id."','catalog/demo/".$photo5.".jpg')";
                mysqli_query($conn, $query_photo5);
                }
                if($photo6 != ''){
                $query_photo6 = "INSERT INTO oc_product_image(product_id, image) VALUES('".$product_id."','catalog/demo/".$photo6.".jpg')";
                mysqli_query($conn, $query_photo6);
                }
                if($photo7 != ''){
                $query_photo7 = "INSERT INTO oc_product_image(product_id, image) VALUES('".$product_id."','catalog/demo/".$photo7.".jpg')";
                mysqli_query($conn, $query_photo7);
                }
                if($photo8 != ''){
                $query_photo8 = "INSERT INTO oc_product_image(product_id, image) VALUES('".$product_id."','catalog/demo/".$photo8.".jpg')";
                mysqli_query($conn, $query_photo8);
                }
        //parent_prod
	
        
        echo '</tr>';
    }echo '</table>';
}
?>