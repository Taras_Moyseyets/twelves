<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$info=$this->db->query("SELECT * FROM oc_main_image WHERE `id`='1'");
		$data['name']=$info->rows[0]['name'];
		$data['button_text']=$info->rows[0]['button_text'];
		$data['href']=$info->rows[0]['href'];
		$data['thumb']="image/".$info->rows[0]['image'];

		$cat=$this->db->query("SELECT * FROM oc_category");
		$cat_description=$this->db->query("SELECT * FROM oc_category_description");
		$category=array();

		foreach($cat->rows as $key=>$value)
		{
			$category[$key]['id']=$cat->rows[$key]['category_id'];
			$category[$key]['name']=$cat_description->rows[$key]['name'];
			$category[$key]['image']=$cat->rows[$key]['image'];
			$category[$key]['href']="index.php?route=product/category&path=".$category[$key]['id'];
		}
		$this->load->model('vblog/blog');
		$data['vblog'] = $this->model_vblog_blog->viewAllPosts();
        foreach($data['vblog'] as $key => $post){
            $data['vblog'][$key]['text'] = $this->model_vblog_blog->cut_text($post['text']);
            //echo $data['vblog'][$key]['title'].'-------------';
            //$data['vblog'][$key]['title'] = $this->model_vblog_blog->del_html($post['title']);
        }

		$data['vblog_link'] = $this->url->link('vblog/blog');

		$data['category']=$category;
		$instagram=json_decode(file_get_contents("https://api.instagram.com/v1/users/self/media/recent/?access_token=3651703019.1677ed0.100bafe60d964c099856723250d2677e"),1);
		$instagram_image=array();
		if(!empty($instagram))
		foreach($instagram['data'] as $key=>$image){
            if($key==12)
            	break;
            $instagram_image[]=array("image"=>$image['images']['low_resolution']['url'],"link" =>$image['link']);
		}
		$data['instagram']=$instagram_image;
		$this->response->setOutput($this->load->view('common/home', $data));
	}
}