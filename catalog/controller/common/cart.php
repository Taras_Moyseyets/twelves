<?php
class ControllerCommonCart extends Controller {
    private $data=array();
	public function index() {
		$this->load->language('common/cart');

		// Totals
		$this->load->model('extension/extension');

		$totals = array();
		$taxes = $this->cart->getTaxes();
		$total = 0;

		// Because __call can not keep var references so we put them into an array.
		$total_data = array(
			'totals' => &$totals,
			'taxes'  => &$taxes,
			'total'  => &$total
		);
			
		// Display prices
		if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);

					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				}
			}

			$sort_order = array();

			foreach ($totals as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $totals);
		}

		$this->data['text_empty'] = $this->language->get('text_empty');
		$this->data['text_cart'] = $this->language->get('text_cart');
		$this->data['text_checkout'] = $this->language->get('text_checkout');
		$this->data['text_recurring'] = $this->language->get('text_recurring');
		$this->data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
		$this->data['text_loading'] = $this->language->get('text_loading');

		$this->data['button_remove'] = $this->language->get('button_remove');

		$this->load->model('tool/image');
		$this->load->model('tool/upload');

		$this->data['products'] = array();

		foreach ($this->cart->getProducts() as $product) {
			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
			} else {
				$image = '';
			}

			$option_data = array();

			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
					'type'  => $option['type']
				);
			}

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
				
				$price = $this->currency->format($unit_price, $this->session->data['currency']);
				$total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
			} else {
				$price = false;
				$total = false;
			}
            $parent=array();
              $product_id=$product['product_id'];
			$this->load->model("catalog/product");
            $sku="";
            $product_sku = $this->model_catalog_product->getProduct($product_id);
    

            $value=$product_sku['sku'];
           	$sku=$this->db->query("SELECT * FROM oc_product_parent WHERE product_sku='".$value."'");
          
            if($sku)
            {
                $product_id=$sku->row['product_id'];
            }
                        
            
          
			$parent_products = $this->model_catalog_product->getParentProducts($product_id);

            $attr = $this->model_catalog_product->getProductAttributes($product_id);
        	foreach($attr as $atr)
        	{
  	         if($atr['name']=="Цвет") 
			         {
				$parent[]=array('color'=>$this->model_catalog_product->translit($atr['attribute'][0]['text']),
								'product_id'=>$product_id);
		          	}
			}
            
			foreach($parent_products as $product2)
			{
				$parent[]=$this->model_catalog_product->getProductBySKU($product2['product_sku']);
			}
  
			$product_info = $this->model_catalog_product->getProduct($product['product_id']);
			$this->data['products'][] = array(
				'cart_id'   => $product['cart_id'],
                'product_id'=> $product['product_id'],
                'parent'    => $parent,
				'thumb'     => $image,
				'name'      => $product['name'],
				'model'     => $product['model'],
				'option'    => $option_data,
				'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
				'quantity'  => $product['quantity'],
				'price'     => $price,
				'total'     => $total,
				'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id']),
				'manufacturer'=>$product_info['manufacturer']
			);
		}

		// Gift Voucher
		$this->data['vouchers'] = array();

		if (!empty($this->session->data['vouchers'])) {
			foreach ($this->session->data['vouchers'] as $key => $voucher) {
				$this->data['vouchers'][] = array(
					'key'         => $key,
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency'])
				);
			}
		}

		$this->data['totals'] = array();

		foreach ($totals as $total) {
			$this->data['totals'][] = array(
				'title' => $total['title'],
				'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
			);
		}

		$this->data['cart'] = $this->url->link('ncheck/check');
		$this->data['checkout'] = $this->url->link('ncheck/check', '', true);

		return $this->load->view('common/cart', $this->data);
	}

	public function info() {
		$this->response->setOutput($this->index());
	}
    
    public function content() {
        $this->index();
		$this->response->setOutput($this->load->view('common/cart_content', $this->data));
	}
}
