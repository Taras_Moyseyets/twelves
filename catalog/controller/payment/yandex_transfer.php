<?php
class ControllerPaymentYandexTransfer extends Controller {
	protected function index() {
		$this->language->load('payment/yandex_transfer');

		$data['text_instruction'] = $this->language->get('text_instruction');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_instruction_2'] = str_replace('{server}', HTTPS_SERVER, $this->language->get('text_instruction_2'));
		$data['text_instruction_3'] = $this->language->get('text_instruction_3');
		$data['text_payment'] = $this->language->get('text_payment');
        $data['text_printpay'] = $this->language->get('text_printpay');

		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['bank'] = nl2br($this->config->get('yandex_transfer_bank_' . $this->config->get('config_language_id')));

		$data['continue'] = $this->url->link('checkout/success');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/yandex_transfer.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/yandex_transfer.tpl';
		} else {
			$this->template = 'default/template/payment/yandex_transfer.tpl';
		}

		$this->render();
	}


    public function printpay() {

        $this->load->model('checkout/order');
        
        $this->language->load('payment/yandex_transfer');
        
        $data['text_instruction'] = $this->language->get('text_instruction');
        $data['text_payment'] = $this->language->get('text_payment');
        
        $data['button_confirm'] = $this->language->get('button_confirm');
        $data['button_back'] = $this->language->get('button_back');
        
        $data['bank'] = nl2br($this->config->get('yandex_transfer_bank_' . $this->config->get('config_language_id')));
        $data['inn'] = nl2br($this->config->get('yandex_transfer_inn_' . $this->config->get('config_language_id')));
        $data['rs'] = nl2br($this->config->get('yandex_transfer_rs_' . $this->config->get('config_language_id')));
        $data['bankuser'] = nl2br($this->config->get('yandex_transfer_bankuser_' . $this->config->get('config_language_id')));
        $data['bik'] = nl2br($this->config->get('yandex_transfer_bik_' . $this->config->get('config_language_id')));
        $data['ks'] = nl2br($this->config->get('yandex_transfer_ks_' . $this->config->get('config_language_id')));
        
        
        $this->load->model('account/order');
        if(isset($this->request->get['order_id']))
        	$order_info = $this->model_account_order->getOrder($this->request->get['order_id']);
        else
            $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        /*	$rur_code = 'RUB';
        $rur_order_total = $this->currency->convert($order_info['total'], $order_info['currency_code'], $rur_code);*/
        

        $data['order_id'] = $order_info['order_id'];

        $data['name'] = $order_info['payment_firstname'] . ' ' . $order_info['payment_lastname'];

        if (!$order_info['payment_address_2']) {
        	$data['address'] = $order_info['payment_zone'] . ', ' . $order_info['payment_city'] . ', ' .$order_info['payment_address_1'] ;
        } else {
        	$data['address'] = $order_info['payment_zone'] . ', ' . $order_info['payment_address_2'] . ', ' . $order_info['payment_city'] . ', ' .$order_info['payment_address_1'] ;
        }
		//print_r($order_info);
        $data['amount'] = $order_info['total'];
        $data['postcode'] = $order_info['payment_postcode'];
        $data['confirm_url'] = $this->url->link('payment/yandex_transfer/confirm');
        foreach ($this->cart->getProducts() as $product)
            $data['products'][] = $product['name'];
        $data['products_names'] = implode(',',$data['products']);
        if(isset($this->request->get['ajax'])){
            echo $this->load->view('payment/yandex_transfer_printpay', $data);
            exit;
        }
        $this->response->setOutput($this->load->view('payment/yandex_transfer_printpay', $data));        
	}


	public function confirm() {
        //file_put_contents('payment.txt',json_encode($_REQUEST));
		$this->language->load('payment/yandex_transfer');

        $this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($_POST['label']);
		$secret = 'bsVDUtX2b9r+7hRldaogBbLm'; // секрет, который мы получили в первом шаге от яндекс.
// получение данных.
        $r = array(
        	'notification_type' => $_POST['notification_type'], // p2p-incoming / card-incoming - с кошелька / с карты
        	'operation_id'      => $_POST['operation_id'],      // Идентификатор операции в истории счета получателя.
        	'amount'            => $_POST['amount'],            // Сумма, которая зачислена на счет получателя.
        	'withdraw_amount'   => $_POST['withdraw_amount'],   // Сумма, которая списана со счета отправителя.
        	'currency'          => $_POST['intval'],            // Код валюты — всегда 643 (рубль РФ согласно ISO 4217).
        	'datetime'          => $_POST['datetime'],          // Дата и время совершения перевода.
        	'sender'            => $_POST['sender'],            // Для переводов из кошелька — номер счета отправителя. Для переводов с произвольной карты — параметр содержит пустую строку.
        	'codepro'           => $_POST['codepro'],           // Для переводов из кошелька — перевод защищен кодом протекции. Для переводов с произвольной карты — всегда false.
        	'label'             => $_POST['label'],             // Метка платежа. Если ее нет, параметр содержит пустую строку.
        	'sha1_hash'         => $_POST['sha1_hash']          // SHA-1 hash параметров уведомления.
        );
        
        // проверка хеш
        if (sha1($r['notification_type'].'&'.
                 $r['operation_id'].'&'.
                 $r['amount'].'&'.
                 $r['currency'].'&'.
                 $r['datetime'].'&'.
                 $r['sender'].'&'.
                 $r['codepro'].'&'.
                 $secret.'&'.
                 $r['label']) != $r['sha1_hash']) {
        	exit('Верификация не пройдена. SHA1_HASH не совпадает.'); // останавливаем скрипт. у вас тут может быть свой код.
        }
        $this->model_checkout_order->addOrderHistory($_POST['label'], $this->config->get('pp_standard_completed_status_id'));
	}
}