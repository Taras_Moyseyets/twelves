<?php
class ControllerVblogBlog extends Controller {
    public function index(){
        $this->load->model('vblog/blog');
        $this->language->load('vblog/blog');
		$limit=4;
		$size_img=array(
						"width"=>859,
						"height"=>500);
		$product_total=$this->model_vblog_blog->countPost();
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
			$count=($page-1)*$limit;
			$data['content'] = $this->model_vblog_blog->viewAllPosts($size_img,$limit,$count);
		} else {
			$page = 1;
			$data['content'] = $this->model_vblog_blog->viewAllPosts($size_img,$limit,0);
		}
		$this->load->model('design/advertise');
		$data['banner']=$this->model_design_advertise->getAllBanners();
		$data['last_news'] = $this->model_vblog_blog->viewAllPosts($size_img,6,0);
        foreach($data['last_news'] as $key => $post)
            $data['last_news'][$key]['title'] = $this->model_vblog_blog->del_html($post['title']);
        foreach($data['content'] as $key=>$post){
            $data['content'][$key]['text'] = $this->model_vblog_blog->cut_text($post['text']);
            $data['content'][$key]['title'] = $this->model_vblog_blog->del_html($post['title']);
        }
		
		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('vblog/blog', 'page={page}');

		$data['pagination'] = $pagination->render();
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));
        $data['heading_title'] = $this->language->get('heading_title');
        $this->response->setOutput($this->load->view('vblog/blog', $data));
    }
	
    public function post(){
        $data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

        $this->load->model('vblog/blog');
        $this->language->load('vblog/blog');
        $id = $this->request->get['post_id'];
		
		$data['post'] = $this->model_vblog_blog->getInfoPost($id);
        $data['post']['time'] = date('d/m/Y',$data['post']['time']);
	   $instagram=json_decode(file_get_contents("https://api.instagram.com/v1/users/self/media/recent/?access_token=3651703019.1677ed0.100bafe60d964c099856723250d2677e"),1);
	
		$instagram_image=array();
		if(!empty($instagram))
		foreach($instagram['data'] as $key=>$image){
			if($key>=4)
                break;
            $instagram_image[] = array("image"=>$image['images']['low_resolution']['url'],"link" =>$image['link']);
		}
        $data['last_news'] = $this->model_vblog_blog->viewAllPosts(0,1,0);
        $data['last_news'] = $data['last_news'][0];
        $data['last_news']['text'] = $this->model_vblog_blog->cut_text($data['last_news']['text']);

		$data['instagram'] = $instagram_image;
        $data['post']['text'] = htmlspecialchars_decode($data['post']['text']);
        $data['post']['title'] = $this->model_vblog_blog->del_html($data['post']['title']);
		$this->response->setOutput($this->load->view('vblog/post', $data));
    }
}
?>