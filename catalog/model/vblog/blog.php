<?php
class ModelVblogBlog extends Model {
    public function getInfoPost($id,$size_img = false){
        $query = $this->db->query("SELECT * FROM vblog WHERE id = ".$id." limit 1");
        $this->load->model('tool/image');
        $query->row['image'] ="image/". $query->row['image'];
        return $query->row;
    }
  
	public function countPost()
	{
		$count = $this->db->query("SELECT COUNT(*) as COUNT FROM vblog");

		return $count->row['COUNT'];
	}
    public function viewAllPosts($size_img = false, $limit=false, $count=false){
		if(!empty($limit))
			$query = $this->db->query("SELECT * FROM vblog ORDER BY time DESC LIMIT ".$count.",".$limit);
		else
			$query = $this->db->query("SELECT * FROM vblog ORDER BY time DESC");
        $this->load->model('tool/image');

		foreach ($query->rows as $key=>$result) {
            $result['time'] = date('m/d',$result['time']);
    		$result['image'] = $this->config->get('config_ssl') . 'image/' . $result['image'];
    		$result['title'] = htmlspecialchars_decode(trim(html_entity_decode($result['title'])));
			$result['href'] = $this->url->link('vblog/blog/post',"post_id=".$result['id']);
            $posts[] = $result;
		}
        if(isset($posts))
            return $posts;
        else
            return array();
    }
    
    public function cut_text($text,$maxchar=100){
        $text = $this->del_html($text);
        $words = explode(' ',trim($text));
        $text="";
        foreach ($words as $word) {
			if (mb_strlen($text.' '.$word)<$maxchar) {
				$text.=' '.$word;
			}
			else {
				$text.='...';
				break;
			}
		}
        return $text;
    }
    
    public function del_html($text){
        return trim(preg_replace('/\\<\/?.*\>/U', '', htmlspecialchars_decode(trim(html_entity_decode($text)))));
    }
}
?>