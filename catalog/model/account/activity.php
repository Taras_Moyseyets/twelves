<?php
class ModelAccountActivity extends Model {
	public function addActivity($key, $data) {
		if (isset($data['customer_id'])) {
			$customer_id = $data['customer_id'];
		} else {
			$customer_id = 0;
		}
		$row=$this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "customer_activity like 'house' ");

		if(!$row->row)
			$this->db->query("ALTER TABLE " . DB_PREFIX . "customer_activity ADD COLUMN house VARCHAR(140) NOT NULL");
			
		$row=$this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "customer_activity like 'flat' ");

		if(!$row->row)
			$this->db->query("ALTER TABLE " . DB_PREFIX . "customer_activity ADD COLUMN flat VARCHAR(140) NOT NULL");
		$this->db->query("INSERT INTO `" . DB_PREFIX . "customer_activity` SET `house`='".$data['house']."',`flat`='".$data['flat']."',`customer_id` = '" . (int)$customer_id . "', `key` = '" . $this->db->escape($key) . "', `data` = '" . $this->db->escape(json_encode($data)) . "', `ip` = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', `date_added` = NOW()");
	}
}