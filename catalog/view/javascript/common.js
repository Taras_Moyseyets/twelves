function getURLVar(key) {
	var value = [];
	var query = String(document.location).split('?');
	if (query[1]) {
		var part = query[1].split('&');
		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');
			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}
		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}
$(document).ready(function() {

	// Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();
		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});
	// Currency
	$('#form-currency .currency-select').on('click', function(e) {
		e.preventDefault();
		$('#form-currency input[name=\'code\']').val($(this).attr('name'));
		$('#form-currency').submit();
	});
	// Language
	$('#form-language .language-select').on('click', function(e) {
		e.preventDefault();
		$('#form-language input[name=\'code\']').val($(this).attr('name'));
		$('#form-language').submit();
	}); /* Search */
	$('#search input[name=\'search\']').parent().keypress(function(event) {
		if (event.which == 13) {
			var url = $('base').attr('href') + 'index.php?route=product/search';
			var value = $('input[name=\'search\']').val();
			if (value) {
				url += '&search=' + encodeURIComponent(value);
			}
			location = url;
		}
	});
	jQuery(document).bind("keyup keydown", function(e) {
		if (e.ctrlKey && e.keyCode == 80) {
			if ($(".forprint img").attr('src') != undefined) {
				var printing_css = '<style media=print>tr:nth-child(even) td{background: #f0f0f0;}</style>';
				$(".forprint img").show();
				var html_to_print = $(".forprint").html();
				console.log(html_to_print);
				var iframe = $('<iframe id="print_frame">');
				$('body').append(iframe);
				var doc = $('#print_frame')[0].contentDocument || $('#print_frame')[0].contentWindow.document;
				var win = $('#print_frame')[0].contentWindow || $('#print_frame')[0];
				doc.getElementsByTagName('body')[0].innerHTML = html_to_print;
				win.print();
				$('iframe').remove();
				$(".forprint img").hide();
			}
		}
	});
	$(".print").click(function() {
		if ($(".forprint")) {
			var printing_css = '<style media=print>tr:nth-child(even) td{background: #f0f0f0;}</style>';
			$(".forprint img").show();
			var html_to_print = $(".forprint").html();
			console.log(html_to_print);
			var iframe = $('<iframe id="print_frame">');
			$('body').append(iframe);
			var doc = $('#print_frame')[0].contentDocument || $('#print_frame')[0].contentWindow.document;
			var win = $('#print_frame')[0].contentWindow || $('#print_frame')[0];
			doc.getElementsByTagName('body')[0].innerHTML = html_to_print;
			win.print();
			$('iframe').remove();
			$(".forprint img").hide();
		}
	});
	$('#search input[name=\'search\']').on('keydown', function(e) {
		if (e.keyCode == 13) {
			$('header #search input[name=\'search\']').parent().find('button').trigger('click');
		}
	});
	// Menu
	$('#menu .dropdown-menu').each(function() {
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();
		var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());
		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 10) + 'px');
		}
	});
	// Product List
	$('#list-view').click(function() {
		$('#content .product-grid > .clearfix').remove();
		$('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');
		$('#grid-view').removeClass('active');
		$('#list-view').addClass('active');
		localStorage.setItem('display', 'list');
	});
	// Product Grid
	$('#grid-view').click(function() {
		// What a shame bootstrap does not take into account dynamically loaded columns
		var cols = $('#column-right, #column-left').length;
		if (cols == 2) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-6');
		} else if (cols == 1) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-6');
		} else {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-6');
		}
		$('#list-view').removeClass('active');
		$('#grid-view').addClass('active');
		localStorage.setItem('display', 'grid');
	});
	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
		$('#list-view').addClass('active');
	} else {
		$('#grid-view').trigger('click');
		$('#grid-view').addClass('active');
	}
	// Checkout
	$(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function(e) {
		if (e.keyCode == 13) {
			$('#collapse-checkout-option #button-login').trigger('click');
		}
	});
	// tooltips on hover
	$('[data-toggle=\'tooltip\']').tooltip({
		container: 'body'
	});
	// Makes tooltips work on ajax generated content
	$(document).ajaxStop(function() {
		$('[data-toggle=\'tooltip\']').tooltip({
			container: 'body'
		});
	});
	// var $container = $('.my-row');
	// 	// Инициализация
	// 	$container.masonry({
	// 	  itemSelector: '.mass-class'
	// 	});
	//  $(".mass-class").each(function (index, el) {
	//  	var img_ = $(el).find('img');
	//      	console.log(img_.width());
	// });
	$(".mass-class").load(function() {
		var img_ = $(this).find('img');
		console.log(img_);
	});
	// $(".mass-class").each(function (i) {
	//      		var img_ = $(this).find('img');
	//      		console.log(img_.width());
	//      		// $(this).width(img_.css('width'));
	//     	});
	$('.sidebar-filter-item > span').click(function() {
		if ($(this).parent().hasClass("active")) {
			$(this).parent().removeClass('active');
		} else {
			$(this).parent().addClass('active');
		}
		$(this).parent().find('ul').toggle("slow");
		$(this).parent().find('form').toggle("slow");
	});
	$('.sidebar-filter-item > .filter-arrow').click(function() {
		if ($(this).parent().hasClass("active")) {
			$(this).parent().removeClass('active');
		} else {
			$(this).parent().addClass('active');
		}
		$(this).parent().find('ul').toggle("slow");
		$(this).parent().find('form').toggle("slow");
	});
	$('.selected-item span').click(function() {
		if ($(this).parent().hasClass("active")) {
			$(this).parent().removeClass('active');
		} else {
			$(this).parent().addClass('active');
		}
		$(this).parent().find('ul').toggle("slow");
	});
	$('.false-select li').click(function() {
		$('.selected-item span').text($(this).text());
		$(this).parent().hide();
		$('.selected-item').removeClass('active');
	});
	$(".color-item label").click(function() {
		var old_id = $(this).attr('old_id');
		var product_id = $(this).attr('product_id');
		$.ajax({
			url: "index.php?route=product/category/getProduct&product_id=" + product_id,
			dataType: "json",
			success: function(data) {
				console.log(data);
				$('#' + old_id + ' .product-thumb .image img').attr("src", data['thumb2']);
				$('#' + old_id + ' .product-thumb .image a').attr("href", data['href']);
				$('#' + old_id + ' .price').text(data['price']);
                $('#' + old_id + ' h4 a').text(data['name']);
				$('#' + old_id + ' .button-group button').attr("onclick", "cart.add('" + product_id + "');");
				//$('#' + old_id + ' .color-item label').attr("old_id", product_id);
				//$('#' + old_id).attr("id", product_id);
			}
		});
		return false;
	})
/*	var old_cart_id = 0;
	$(".color-item-cart label").click(function() {
		var old_id = $(this).attr('old_id');
		var product_id = $(this).attr('product_id');
		cart_id = $(".parent_" + old_id).val();
		console.log(cart_id);
		cart.remove(cart_id);
		cart.add(product_id);
		setTimeout(function() {
			$.ajax({
				url: "index.php?route=product/category/getProduct&product_id=" + product_id,
				dataType: "json",
				async: true,
				success: function(data) {
					console.log(data['card_id']);
					$('#' + old_id + ' .img-thumbnail').attr("src", data['thumb2']);
					$('#' + old_id + ' .img-thumbnail').attr("data-large", data['thumb2']);
					$('#' + old_id + ' a').attr("href", data['href']);
					$('#' + old_id + ' .product-product-name').text(data['name']);
					$('#' + old_id + ' .product-price').text(data['price']);
					$('#' + old_id + ' .brend').text(data['manufacturer']);
					$('#' + old_id + ' .button-group button').attr("onclick", "cart.add('" + product_id + "');");
					$('#' + old_id + ' .cart-button-exit').attr("onclick", "cart.remove('" + data['card_id'] + "');");
					//$(".parent_" + old_id).val(data['card_id']);
					//$(this).attr("product_id",data['product_id']);
					//location.reload();
					//$('#'+old_id).attr("id",product_id);
				}
			})
		}, 100);
	})*/

	$("body").on("click",".color-item-cart label",function() {
		var old_id = $(this).attr('old_id'),
		product_id = $(this).attr('product_id'),
		cart_id = $(this).attr('cart_id');
	
		$.ajax({
			url: "index.php?route=product/category/getProduct&product_id=" + product_id,
			dataType: "json",
			success: function(data) {
				console.log(data);
				$('#' + old_id + ' .img-thumbnail').attr("src", data['thumb2']);
				$('#' + old_id + ' .img-thumbnail').attr("data-large", data['thumb2']);
				$('#' + old_id + ' a').attr("href", data['href']);
				$('#' + old_id + ' .product-product-name').text(data['name']);
				$('#' + old_id + ' .product-price').text(data['price']);
				$('#' + old_id + ' .brend').text(data['manufacturer']);
				$('#' + old_id + ' .button-group button').attr("onclick", "cart.add('" + product_id + "');");
				$('#' + old_id + ' .color-item label').attr("old_id", product_id);
				cart.add(product_id);
				cart.remove(cart_id);
				//$('#'+old_id).attr("id",product_id);
			}
		});
	})
	$(document).on('click', '.paypal', function() {
		var comment = $("textarea").val();
		name = $("input[name=name]").val();
		address_1 = $("input[name=address_1]").val();
		telephone = $("input[name=telephone]").val();
		email = $("input[name=email]").val();
		house = $("input[name=house]").val();
		flat = $("input[name=flat]").val(), payment = $("[name=payment]").val();
		$.ajax({
			url: "index.php?route=checkout/cart/editOrder&comment=" + comment + "&name=" + name + "&address_1=" + address_1 + "&telephone=" + telephone + "&email=" + email + "&house=" + house + "&flat=" + flat,
			async: true,
			dataType: "json"
		});
		if (payment == "money") {
			$.ajax({
				url: "index.php?route=ajax/ajax/AddOrderHistory",
				async: true,
				complete: function() {
					location.href = "?index.php";
				}
			});
            return false;
		}
	});
	$(".pay").click(function() {
		$.ajax({
			url: "index.php?route=ajax/ajax/ClearCart",
			async: true
		});
		var product_id = $("#button-cart1").attr('product_id');
		cart.add(product_id);
		/*$.ajax({
			url: "index.php?route=ajax/ajax/addOrder",
			async: true
		});
		*/
		$.ajax({
			url: "index.php?route=ncheck/check",
			async: true,
		});
        //return false;
		var comment = $(".comment").val();
		name = $("input[name=first_name]").val() + " " + $("input[name=last_name]").val();
		address_1 = $("input[name=address_1]").val();
		telephone = $("input[name=telephone]").val();
		email = $("input[name=email]").val();
		house = $("input[name=house]").val();
		flat = $("input[name=flat]").val();
		$.ajax({
			url: "index.php?route=checkout/cart/editOrder&comment=" + comment + "&name=" + name + "&address_1=" + address_1 + "&telephone=" + telephone + "&email=" + email + "&house=" + house + "&flat=" + flat,
			dataType: "json",
			async: true,
		});
		
		if ($("[name=payment]").val() == "paypal")
			$.ajax({
			url: "index.php?route=ajax/ajax/getPayPal",
			async: true,
			success: function(data) {
				$("form .for_pp").append(data);
				$(".paypal").click();
			}
		});
		else if ($("[name=payment]").val() == "yandex")
			$.ajax({
			url: "index.php?route=payment/yandex_transfer/printpay&ajax=1",
			async: true,
			success: function(data) {
				$("form .for_pp").append(data);
				$(".widget-small__button").click();
			}
		});
		else if ($("[name=payment]").val() == "money")
			$.ajax({
			url: "index.php?route=ajax/ajax/AddOrderHistory",
			async: true,
			success: function(data) {
				$("#bay-one-click-two-window").html("<h1>Спасибо за размещение заказа. В ближайшее время с Вами свяжется наш сотрудник для уточнения деталей</h1>");
				$(".fancybox-inner").height(200);
				return true;
			}
		});
		return false;
	});
	$(".color-item-header-cart label").click(function() {
		var old_id = $(this).attr('old_id');
		product_id = $(this).attr('product_id');
		cart_id = $(this).attr('cart_id');
		$.ajax({
			url: "index.php?route=product/category/getProduct&product_id=" + product_id,
			dataType: "json",
			success: function(data) {
				console.log(data);
				$('#' + old_id + ' .img-thumbnail').attr("src", data['thumb_120_178']);
				//$('#'+old_id+' .img-thumbnail').attr("data-large",data['thumb_header']);
				$('#' + old_id + ' a').attr("href", data['href']);
				$('#' + old_id + ' .product-product-name').text(data['name']);
				$('#' + old_id + ' .product-price').text(data['price']);
				$('#' + old_id + ' .brend').text(data['manufacturer']);
				$('#' + old_id + ' .button-group button').attr("onclick", "cart.add('" + product_id + "');");
				$('#' + old_id + ' .color-item label').attr("old_id", product_id);
				//$('#'+old_id).attr("id",product_id);
			}
		});
		return false;
	})
	$(".color-item-review").on("click", "label", function() {
		var product_id = $(this).attr('product_id');
		$.ajax({
			url: "index.php?route=product/category/getProduct&product_id=" + product_id,
			dataType: "json",
			success: function(data) {
				console.log(data);
				$(".preview").find("a.brend").text(data['manufacturer']);
				$(".preview").find("a.brend").attr("href", data['href']);
				$(".preview .list-unstyled h1").text(data['name']);
				$(".preview .main-photo img").attr("src", data['thumb']);
				$(".price-block h2").text(data['price']);
				if (data['special']) $(".price-block h2").text(data['special']);
				$("#tab-description").text(data['description']);
				$(".preview .more").attr("href", data['href']);
				$("#tab-specification table").html("");
				$(".main-photo .discount").html("");
				$(".preview .color-item-review").html($("#" + product_id + " .color-item").html());
				if (data['special_discount']) $(".main-photo .discount").append("скидка <span>" + data['special_discount'] + "%</span>");
				else if (data['new_product']) $(".main-photo .discount").append("Новинка");
				else if (data['date_available']) $(".main-photo .discount").append("Ожидается с  с <span>" + data['date_available'] + "</span>");
				$(".more").attr("href", data['href']);
				$("#tab-review").html(data['category_delivery']);
				$("#tab-specification .table").empty();
				for (var i = 0; i < data['attribute_groups'].length; i++) {
					// $("#tab-specification table").append('<thead><tr><td colspan="+2+"><strong>'+data['attribute_groups'][i]['name']+'</strong></td></tr><thead>');
					for (var j = 0; j < data['attribute_groups'][i]['attribute'].length; j++)
					$("#tab-specification .table").append('<div class="tr"><div class="td">' + data['attribute_groups'][i]['attribute'][j]['name'] + ':</div><div class="td">' + data['attribute_groups'][i]['attribute'][j]['text'] + '</div></div>');
				}
                $(".preview .cover-vs").html('');
    			if (data['images'])
    				$('.preview .cover-vs').html('<span class="verical-slider"></span><a class="vertical-tools up" id="up"></a><a class="vertical-tools down " id="down"></a>');
				
				$(".preview .thumbnails .image-additional").remove();
    			for (var i = 0; i < data['images'].length; i++) {
    				$(".thumbnails .verical-slider").append('<li class="image-additional"><a href="#"><img src="' + data['images'][i]['popup'] + '" src="' + data['images'][i]['thumb'] + '" title="" alt="" /></a></li>');
    			}

    			$(".thumbnails .verical-slider").append('<li class="image-additional"><a href="#"><img src="' + data['thumb'] + '" title="" alt="" /></a></li>');
                
                var s_type;
    			if ($(document).width() < 768) {
    				s_type = 'horizontal';
    			} else {
    				s_type = 'vertical';
    			}
    			slider = $('.preview .verical-slider').bxSlider({
    				mode: s_type,
    				infiniteLoop: true,
    				minSlides: 3,
    				// maxSlides: 3,
    				pager: false,
    				controls: false,
    				moveSlides: 1
    			});
                console.log(slider);
    			$('.thumbnails').on('click', '.verical-slider a', function(event) {
    				event.preventDefault();
    				$('.image-additional a').css('border', '1px solid transparent');
    				$(this).css('border', '1px solid #000');
    				slider.goToNextSlide();
    			});
                /* if(data['images'].length>3) {
                $(".thumbnails .cover-vs").append('<a class="vertical-tools up" id="up"></a><a class="vertical-tools down " id="down"></a>');
                }*/
                
                
    			$('.preview .thumbnails').on('click', 'a.down', function() {
    				slider.goToPrevSlide();
    				$('.image-additional a').css('border', '1px solid transparent');
    				return false;
    			});
    			$('.preview .thumbnails').on('click', 'a.up', function() {
    				slider.goToNextSlide();
    				$('.image-additional a').css('border', '1px solid transparent');
    				return false;
    			});
			}
		});
	})
	$("[href=#bay-one-click-two-window]").click(function() {
		console.log("bay");
		$("[name=telephone]").val($("[name=first_telephone]").val());
		$("[name=first_name]").val($("[name=first_name_first]").val());
		$("[name=email]").val($("[name=first_mail]").val());
	})
	$('.product-layout .eye').click(function() {
		var product_id = $(this).parent().attr('id');
		var slider;
		$.ajax({
			url: "index.php?route=product/category/getProduct&product_id=" + $(this).parent().attr('id'),
			dataType: "json",
			success: function(data) {
				console.log(data);
				$(".preview").find("a.brend").text(data['manufacturer']);
				$(".preview").find("a.brend").attr("href", data['href']);
				$(".preview .list-unstyled h1").text(data['name']);
				$(".preview .main-photo img").attr("src", data['thumb']);
				$(".star-button").attr("onclick", "wishlist.add(" + data['product_id'] + ");");
				$(".price-block h2").text(data['price']);
				if (data['special']) $(".price-block h2").text(data['special']);
				$("#tab-description").text(data['description']);
				$(".preview .more").attr("href", data['href']);
				$("#tab-specification table").html("");
				$(".main-photo .discount").html("");
				$(".preview .color-item-review").html($("#content #" + product_id + " .color-item").html());
				if (data['special_discount']) $(".main-photo .discount").append("скидка <span>" + data['special_discount'] + "%</span>");
				else if (data['new_product']) $(".main-photo .discount").append("Новинка");
				else if (data['date_available']) $(".main-photo .discount").append("Ожидается с  с <span>" + data['date_available'] + "</span>");
				$(".more").attr("href", data['href']);
				$("#tab-review").html(data['category_delivery']);
				$("#tab-specification .table").empty();
				for (var i = 0; i < data['attribute_groups'].length; i++) {
					// $("#tab-specification table").append('<thead><tr><td colspan="+2+"><strong>'+data['attribute_groups'][i]['name']+'</strong></td></tr><thead>');
					for (var j = 0; j < data['attribute_groups'][i]['attribute'].length; j++)
					$("#tab-specification .table").append('<div class="tr"><div class="td">' + data['attribute_groups'][i]['attribute'][j]['name'] + ':</div><div class="td">' + data['attribute_groups'][i]['attribute'][j]['text'] + '</div></div>');
				}

				
  				$(".preview .cover-vs").html('');
    			if (data['images'])
    				$('.preview .cover-vs').html('<span class="verical-slider"></span><a class="vertical-tools up" id="up"></a><a class="vertical-tools down " id="down"></a>');
				
				$(".preview .thumbnails .image-additional").remove();
    			for (var i = 0; i < data['images'].length; i++) {
    				$(".thumbnails .verical-slider").append('<li class="image-additional"><a href="#"><img src="' + data['images'][i]['popup'] + '" src="' + data['images'][i]['thumb'] + '" title="" alt="" /></a></li>');
    			}
    			$(".thumbnails .verical-slider").append('<li class="image-additional"><a href="#"><img src="' + data['thumb'] + '" title="" alt="" /></a></li>');
    			
                
                var s_type;
    			if ($(document).width() < 768) {
    				s_type = 'horizontal';
    			} else {
    				s_type = 'vertical';
    			}
    			slider = $('.preview .verical-slider').bxSlider({
    				mode: s_type,
    				infiniteLoop: true,
    				minSlides: 3,
    				// maxSlides: 3,
    				pager: false,
    				controls: false,
    				moveSlides: 1
    			});
                console.log(slider);
    			$('.thumbnails').on('click', '.verical-slider a', function(event) {
    				event.preventDefault();
    				$('.image-additional a').css('border', '1px solid transparent');
    				$(this).css('border', '1px solid #000');
    				slider.goToNextSlide();
    			});
                /* if(data['images'].length>3) {
                $(".thumbnails .cover-vs").append('<a class="vertical-tools up" id="up"></a><a class="vertical-tools down " id="down"></a>');
                }*/
                
                
    			$('.preview .thumbnails').on('click', 'a.down', function() {
    				slider.goToPrevSlide();
    				$('.image-additional a').css('border', '1px solid transparent');
    				return false;
    			});
    			$('.preview .thumbnails').on('click', 'a.up', function() {
    				slider.goToNextSlide();
    				$('.image-additional a').css('border', '1px solid transparent');
    				return false;
    			});
							
				$('.back-light').show();
				$('.preview').show();
				
				if(data['wish_class'] != ''){
					$(".star-button .pr2-icon").addClass(data['wish_class']);
					console.log(data['wish_class']);
				}
			}
		});
	});
	$('.exit3').click(function() {
		$('.back').hide();
		$('.preview').hide();
		$(".star-button .pr2-icon").removeClass('icon-pr2-star-active');
		$(".preview .thumbnails .bx-wrapper").remove();
	});
	$('#nav-toggle').click(function() {
		$('.header-menu-mobile').toggle('slow');
	});
	$(window).scroll(function() {
		if ($(this).scrollTop() > 53) {
			$('#logo').css({
				'width': '100px',
				'margin-left': '-50px'
			});
			$('#top').css({
				'z-index': '1200'
			});
		} else {
			$('#logo').css({
				'width': 'inherit',
				'margin-left': '-121px'
			});
			$('#top').css({
				'z-index': '1000'
			});
		}
	});
	$(window).on("load", function() {
		$(".sidebar-filter-item > ul ").mCustomScrollbar({
			theme: "rounded"
		});
	});
	//   	jQuery('.arrow-up').mouseover( function(){
	// 	jQuery( this ).animate({opacity: 0.65},500);
	// }).mouseout( function(){
	// 	jQuery( this ).animate({opacity: 1},500);
	// }).click( function(){
	// 	window.scroll(0 ,500); 
	// 	return false;
	// });
	$(".arrow-up").on("click", "a", function(event) {
		//отменяем стандартную обработку нажатия по ссылке
		event.preventDefault();
		//анимируем переход на расстояние - top за 1500 мс
		$('body,html').animate({
			scrollTop: 0
		}, 1000);
	});
	jQuery(window).scroll(function() {
		if (jQuery(document).scrollTop() > 0) {
			jQuery('.arrow-up').fadeIn('fast');
		} else {
			jQuery('.arrow-up').fadeOut('fast');
		}
	});
	// $('.back').click(function(){
	// 	$('.back').hide();
	// }); 
	$('#search-button').click(function(event) {
		event.preventDefault();
		$(".shadow").show();
		$("#search").show();
	});
	$(".shadow").click(function() {
		$("#search").hide();
		$(".shadow").hide();
	});
	$('#search .exit3').click(function() {
		$('#search input[type="text"]').val('').focus();
	});
	$('.mass-class img').load(function() {
		$('.my-row').masonry({
			// options
			itemSelector: '.mass-class',
			singleMode: true,
			isResizable: true,
			isAnimated: true,
			animationOptions: {
				queue: false,
				duration: 500
			}
		});
	});
	$(".main-photo img").mouseenter(function() {
		if ($(document).width() > 992) {
			$(".nx-product .main-photo img").imagezoomsl({
				zoomrange: [1, 10],
				magnifiereffectanimate: "fadeIn",
				rightoffset: 80,
				magnifierborder: 'none'
			});
		}
	});
	$(document).resize(function() {
		if ($(document).width() > 992) {
			$(".nx-product .main-photo img").imagezoomsl({
				zoomrange: [1, 10],
				magnifiereffectanimate: "fadeIn",
				rightoffset: 100,
				magnifierborder: 'none'
			});
		}
	});
	$(document).resize(function() {
		console.log("resize");
		if ($(document).width() < 768) {
			var s_type;
			if ($(document).width() < 768) {
				s_type = 'horizontal';
			} else {
				s_type = 'vertical';
			}
			var slider = $('.verical-slider').bxSlider({
				mode: s_type,
				infiniteLoop: true,
				minSlides: 3,
				// maxSlides: 3,
				slideMargin: 20,
				pager: false,
				controls: false,
				moveSlides: 1
			});
		}
	});
	var s_type;
	if ($(document).width() < 768) {
		s_type = 'horizontal';
	} else {
		s_type = 'vertical';
	}

	slider = $('.verical-slider').bxSlider({
		mode: s_type,
		infiniteLoop: true,
		minSlides: 3,
		// maxSlides: 3,
		slideMargin: 20,
		pager: false,
		controls: false,
		moveSlides: 1
	});
	$('.down').click(function() {
		slider.goToPrevSlide();
		return false;
	});
	$('.up').click(function() {
		slider.goToNextSlide();
		return false;
	});
	$('.thumbnails').on('click', '.verical-slider a', function(event) {
		event.preventDefault();
		$('.image-additional a').css('border', '1px solid transparent');
		$(this).css('border', '1px solid #000');
		var src_img = $(this).find('img').attr('src');
		$('.main-photo img').attr('src', src_img);
		$('.main-photo img').attr('data-large', src_img);
		slider.goToNextSlide();
	});
	$('.exit').click(function() {
		$('.back').hide();
		$('.social-block').hide();
		$('.product-added').hide();
	});
	$('.exit2').click(function() {
		$('.back').hide();
		$('.social-block').hide();
		$('.product-added').hide();
	});
	$('.addto-cart-two').click(function() {
		$('.back').show();
		$('.product-added').show();
		var img = $(this).parent().parent().find('img');
		console.log(img);
		var brand = $(this).parent().parent().find('.brend').text();
		var pr = $(this).parent().parent().find('a').text();
		$('.product-added img').attr('src', img.attr('src'));
		$('.product-added .brend').text(brand);
		$('.product-added h1').text(pr);
		$('.exit').hide();
		setTimeout(func, 40000);

		function func() {
			$('.back').hide();
			$('.social-block').hide();
			$('.product-added').hide();
		}
	});
	$('.addto-cart-second').click(function() {
		$('.back').show();
		$('.product-added').show();
		$('.preview').hide();
		var img = $(this).parent().parent().parent().find('img');
		console.log(img);
		var brand = $(this).parent().parent().find('.brend').text();
		var pr = $(this).parent().parent().find('a').text();
		$('.product-added img').attr('src', img.attr('src'));
		$('.product-added .brend').text(brand);
		$('.product-added h1').text(pr);
		$('.exit').hide();
		setTimeout(func, 40000);

		function func() {
			$('.back').hide();
			$('.social-block').hide();
			$('.product-added').hide();
		}
	});
	$('.addto-cart-third').click(function() {
		$('.back').show();
		$('.product-added').show();
		$('.preview').hide();
		var img = $('main-photo img');
		console.log(img);
		var brand = $(this).parent().parent().find('.brend').text();
		var pr = $(this).parent().parent().find('h1').text();
		$('.product-added img').attr('src', img.attr('src'));
		$('.product-added .brend').text(brand);
		$('.product-added h1').text(pr);
		$('.exit').hide();
		setTimeout(func, 40000);

		function func() {
			$('.back').hide();
			$('.social-block').hide();
			$('.product-added').hide();
		}
	});
	$(window).on("load", function() {
		//       $(".b-slider").mCustomScrollbar({
		//     axis:"x", // horizontal scrollbar
		//     scrollButtons:{ enable: true },
		//     theme: "3d",
		//      scrollInertia:1000
		// });
		$('.variable-width').slick({
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			centerMode: true,
			variableWidth: true
		});
	});
	$(document).on('click', '.decrease', function() {
		var parent = $(this).parent();
        var val = parent.find('.quantity_product').val();
		var name = parent.find('.quantity_product').attr('name');
		val = parseInt(val) - parseInt(1);
		parent.find('.quantity_product').val(val);
        
		setTimeout(function() {
			$('.icon-cart').addClass('notempty');
			var text = $('#cart-total').text() - 1;
			$('#cart > button').html('<i class="icon icon-cart notempty"><span id="cart-total" class="header-icon-span">' + text + ' </span></i>');
		}, 100);
        
		$(parent).find('.quantity_product').change();
		return false;
	});
	$(document).on('click', '.increase', function() {
		var parent = $(this).parent();
		var val = parent.find('.quantity_product').val();
		val = parseInt(val) + parseInt(1);
		parent.find('.quantity_product').val(val);
		$(parent).find('.quantity_product').change();
		var text = parseInt($('#cart-total').text()) + parseInt(1);
		setTimeout(function() {
			$('.icon-cart').addClass('notempty');
			$('#cart > button').html('<i class="icon icon-cart notempty"><span id="cart-total" class="header-icon-span"> ' + text + ' </span></i>');
		}, 100);
		return false;
	});

	function pay_ajax_send() {
        var paymant_type = $('[name="payment"]').val();
        if(paymant_type != 'yandex' && paymant_type != 'paypal')
            return false;		
		var url = '';
		if (paymant_type == 'yandex') url = "&ajax=1&pay_system=yandex";
		else if (paymant_type == 'paypal') url = "&ajax=1&pay_system=paypal";
		$.ajax({
			url: "index.php?route=ncheck/check"+url,
			dataType: "html",
			success: function(data) {
				$('.paypal_form').html(data);
			}
		});
	}
	$(document).on('change', '[name="payment"]', function() {
		pay_ajax_send();
	});
	$(document).on('change', '.quantity_product', function() {
        var parent_block = $(this).parents(':eq(2)');
		var count = parseFloat($(this).val());
		var money = parseFloat($(parent_block).find('.product-price').text());
		var one_prod = parseFloat($(parent_block).find('[name=one_prod]').val());
		var money_text = (one_prod * count) + '.00 руб.';
		$(parent_block).find('.product-price').text(money_text);
        var name = $(this).attr('name');
        $.ajax({
            async:false,
			url: 'index.php?route=checkout/cart/edit',
			data: name + '=' + count,
			type: 'post',
			dataType: 'json'
		});
		if (count > 0) pay_ajax_send();
		else
		location.reload();
		return false;
	});
	$('#cart button').click(function() {
		$.ajax({
			url: "index.php?route=common/cart/content",
			dataType: "html",
			success: function(data) {
				$('#cart ul').html(data);
			}
		});
		$('.cart-window').show();
		$('.back').show();
		$('.preview').hide();
	});
	$('.back').click(function() {
		$('.cart-window').hide();
		$('.product-added').hide();
		$('.social-block').hide();
		$('.back').hide();
		$('.preview').hide();
	});
	$('.share-button').click(function() {
		$('.back').show();
		$('.social-block').show();
		$('.preview').hide();
	});

	function getUrlVar() {
		var urlVar = window.location.search; // получаем параметры из урла
		var arrayVar = []; // массив для хранения переменных
		var valueAndKey = []; // массив для временного хранения значения и имени переменной
		var resultArray = []; // массив для хранения переменных
		arrayVar = (urlVar.substr(1)).split('&'); // разбираем урл на параметры
		if (arrayVar[0] == "") return false; // если нет переменных в урле
		for (i = 0; i < arrayVar.length; i++) { // перебираем все переменные из урла
			valueAndKey = arrayVar[i].split('='); // пишем в массив имя переменной и ее значение
			resultArray[valueAndKey[0]] = valueAndKey[1]; // пишем в итоговый массив имя переменной и ее значение
		}
		return resultArray; // возвращаем результат
	}
	var result = getUrlVar();
	if (result['information_id']) {
		var m = result['information_id'];
		console.log(result);
		$('.sidebar__menu a[data-id="' + m + '"] ').addClass('active');
	} else {
		$('.about-contacts').addClass('active');
	}
	$('#input-email').focus(function() {
		$(this).removeClass('error-class');
	});
	$('#input-enquiry').focus(function() {
		$(this).removeClass('error-class');
	});
});
// Cart add remove functions
var cart = {
	'add': function(product_id, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			async: true,
			beforeSend: function() {
				$('#cart > button').button('загрузка');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();
				if (json['redirect']) {
					location = json['redirect'];
				}
				if (json['success']) {
					// Need to set timeout otherwise it wont update the total
					setTimeout(function() {
						$('.icon-cart').addClass('notempty');
						var str = json['total'];
						str = str.split('-')[0];
						str = str.replace(/[^-0-9]/gim, '');
						$('#cart > button').html('<i class="icon icon-cart notempty"><span id="cart-total" class="header-icon-span">' + str + ' </span></i>');
					}, 100);
					$('html, body').animate({
						scrollTop: 0
					}, 'slow');
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'update': function(key, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/edit',
			type: 'post',
			data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('загрузка');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function() {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);
				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function() {
					var str = json['total'];
					str = str.split('-')[0];
					str = str.replace(/[^-0-9]/gim, '');
					if (str == '0') {
						$('#cart > button').html('<i class="icon icon-cart"><span id="cart-total" class="header-icon-span"></span></i>');
					} else {
						$('#cart > button').html('<i class="icon icon-cart notempty"><span id="cart-total" class="header-icon-span">' + str + ' </span></i>');
					}
				}, 100);
				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
}
var voucher = {
	'add': function() {},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function() {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);
				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
}
var wishlist = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();
				if (json['redirect']) {
					location = json['redirect'];
				}
				var nx_total = json['total'].replace(/[^-0-9]/gim, '');
				$('#wishlist-total span').html(nx_total);
				$('.star-button i').toggleClass('icon-pr2-star-active');
				$('#wishlist-total').attr('title', json['total']);
				// $('html, body').animate({
				// 	scrollTop: 0
				// }, 'slow');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'remove': function() {}
}
var compare = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=product/compare/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();
				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					$('#compare-total').html(json['total']);
					$('html, body').animate({
						scrollTop: 0
					}, 'slow');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'remove': function() {}
} /* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
	e.preventDefault();
	$('#modal-agree').remove();
	var element = this;
	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';
			$('body').append(html);
			$('#modal-agree').modal('show');
		}
	});
});
// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			this.timer = null;
			this.items = new Array();
			$.extend(this, option);
			$(this).attr('autocomplete', 'off');
			// Focus
			$(this).on('focus', function() {
				this.request();
			});
			// Blur
			$(this).on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);
			});
			// Keydown
			$(this).on('keydown', function(event) {
				switch (event.keyCode) {
				case 27:
					// escape
					this.hide();
					break;
				default:
					this.request();
					break;
				}
			});
			// Click
			this.click = function(event) {
				event.preventDefault();
				value = $(event.target).parent().attr('data-value');
				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			}
			// Show
			this.show = function() {
				var pos = $(this).position();
				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left
				});
				$(this).siblings('ul.dropdown-menu').show();
			}
			// Hide
			this.hide = function() {
				$(this).siblings('ul.dropdown-menu').hide();
			}
			// Request
			this.request = function() {
				clearTimeout(this.timer);
				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			}
			// Response
			this.response = function(json) {
				html = '';
				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i]['value']] = json[i];
					}
					for (i = 0; i < json.length; i++) {
						if (!json[i]['category']) {
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						}
					}
					// Get all the ones with a categories
					var category = new Array();
					for (i = 0; i < json.length; i++) {
						if (json[i]['category']) {
							if (!category[json[i]['category']]) {
								category[json[i]['category']] = new Array();
								category[json[i]['category']]['name'] = json[i]['category'];
								category[json[i]['category']]['item'] = new Array();
							}
							category[json[i]['category']]['item'].push(json[i]);
						}
					}
					for (i in category) {
						html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';
						for (j = 0; j < category[i]['item'].length; j++) {
							html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
						}
					}
				}
				if (html) {
					this.show();
				} else {
					this.hide();
				}
				$(this).siblings('ul.dropdown-menu').html(html);
			}
			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));
		});
	}
	$(document).ready(function() {
		$('#nav-toggle').click(function() {
			$(this).toggleClass('active');
		});
	});
})(window.jQuery);