$(document).ready(function(){
    $(".color-items label").click(function() {
    	$('.color-items input').prop('checked', false);
        $('[name = '+$(this).attr('class')+']').prop('checked',true);
    	$.ajax({
    		url: "index.php?route=product/category/getProduct&product_id=" + $(this).attr('product_id'),
    		dataType: "json",
    		success: function(data) {
    			console.log(data);
    			$("a.brend").text(data['manufacturer']);
    			$("a.brend").attr("href", data['href']);
    			$(".list-unstyled h1").text(data['name']);
    			$(".main-photo img").attr("src", data['thumb3']);
    			$(".main-photo img").attr("data-large", data['thumb3']);
    			$(".price-block h2").text(data['price']);
    			if (data['special']) $(".price-block h2").text(data['special']);
    			$(".cover-vs").html('');
    			if (data['images'])
    				$('.cover-vs').html('<span class="verical-slider"></span><a class="vertical-tools up" id="up"></a><a class="vertical-tools down " id="down"></a>');
    			$("#tab-description").text(data['description']);
    			$(".more").attr("href", data['href']);
    			$("#tab-specification table").html("");
    			$(".main-photo .discount").html("");
    			$("#button-cart").attr("onclick", "cart.add('" + data['product_id'] + "')");
    			if (data['special_discount']) $(".main-photo .discount").append("скидка <span>" + data['special_discount'] + "%</span>");
    			else if (data['new_product']) $(".main-photo .discount").append("Новинка");
    			else if (data['date_available']) $(".main-photo .discount").append("Ожидается с  с " + data['date_available']);
    			$(".more").attr("href", data['href']);
    			$("#tab-review").html(data['category_delivery']);
    			for (var i = 0; i < data['attribute_groups'].length; i++) {
    				$("#tab-specification table").append('<thead><tr><td colspan="+2+"><strong>' + data['attribute_groups'][i]['name'] + '</strong></td></tr><thead>');
    				for (var j = 0; j < data['attribute_groups'][i]['attribute'].length; j++)
    				$("#tab-specification table").append('<tbody><tr><td>' + data['attribute_groups'][i]['attribute'][j]['name'] + '</td><td>' + data['attribute_groups'][i]['attribute'][j]['text'] + '</td></tr><tbody>');
    			}
    			
                
                $(".thumbnails .image-additional").remove();
    			for (var i = 0; i < data['images'].length; i++) {
    				$(".thumbnails .verical-slider").append('<li class="image-additional bx-clone" style="float: none; list-style: none; position: relative; width: 100px; margin-bottom: 20px;"><a href="#"><img src="' + data['images'][i]['popup'] + '" src="' + data['images'][i]['thumb'] + '" title="" alt="" /></a></li>');
    			}
    			var s_type;
    			if ($(document).width() < 768) {
    				s_type = 'horizontal';
    			} else {
    				s_type = 'vertical';
    			}
    			var slider = $('.verical-slider').bxSlider({
    				mode: s_type,
    				infiniteLoop: true,
    				minSlides: 3,
    				// maxSlides: 3,
    				slideMargin: 20,
    				pager: false,
    				controls: false,
    				moveSlides: 1
    			});
    			$('.thumbnails').on('click', '.verical-slider a', function(event) {
    				event.preventDefault();
    				$('.image-additional a').css('border', '1px solid transparent');
    				$(this).css('border', '1px solid #000');
    				slider.goToNextSlide();
    			});
                /* if(data['images'].length>3) {
                $(".thumbnails .cover-vs").append('<a class="vertical-tools up" id="up"></a><a class="vertical-tools down " id="down"></a>');
                }*/
                
                
    			$('.thumbnails').on('click', 'a.down', function() {
    				slider.goToPrevSlide();
    				$('.image-additional a').css('border', '1px solid transparent');
    				return false;
    			});
    			$('.thumbnails').on('click', 'a.up', function() {
    				slider.goToNextSlide();
    				$('.image-additional a').css('border', '1px solid transparent');
    				return false;
    			});
                
    			$(".star-button").attr("onclick", "wishlist.add('" + data['product_id'] + "');")
    		}
    	});
    });
});