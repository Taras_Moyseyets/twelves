$(document).ready(function() {
	$.each($(".form-office input"), function(index, value) {
		if($(value).val() != "") {
			$(this).parent().find("label").addClass("active-label");
		} 
	});
	$(".form-office input").focus(function() {
		$(this).parent().parent().find("label").addClass("active-label");
	});
	$(".form-office input").blur(function() {
		if($(this).val() == "") {
			$(this).parent().parent().find("label").removeClass("active-label");
		}
	});
	$(".window-of-bottom-close").click(function() {
		$(".window-of-bottom").removeClass("window-of-bottom-show");
	})
	function validateEmail(email) {  
	    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}
	$(".send-button-save-options").click(function() {
		var state = 0;
		$.each($('[required]'), function(index, value) {
			if($(value).val() == '') {
				state++;
				$(value).addClass("required-border-color");
				setTimeout(function() {
					$(value).removeClass("required-border-color");
				}, 2000);
				return false;
			}
			if($(value).attr("name") == "email") {
				if(!(validateEmail($(value).val()))) {
					state++;
					$("input[name='email']").addClass("required-border-color");
					setTimeout(function() {
						$("input[name='email']").removeClass("required-border-color");
					}, 2000);
					return false;
				}
			} 
			if($(value).attr("name") == "telephone") {
				if($(value).val().length < 3) {
					state++;
					$(value).addClass("required-border-color");
					setTimeout(function() {
						$(value).removeClass("required-border-color");
					}, 2000);
					return false;
				}
			}
		});
		if(state == 0) {
			$.ajax({
			  type: "POST",
			  url: "index.php?route=account/edit",
			  data: $(".form-office-save-options").serialize(),
			  dataType: "json"
			});
			$(".window-of-bottom").addClass("window-of-bottom-show");
		} 
	});
});