<?php echo $header; ?>
<div class="container-fluid">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row page-info">
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
        <aside id="column-left" class="col-sm-4 col-md-3">
    <ul class="list-unstyled sidebar__menu">
      <li><a data-id="4" href="index.php?route=information/information&information_id=4">О нас</a></li>
      <li><a data-id="6" href="index.php?route=information/information&information_id=8">Оферта</a></li>
      <li><a class="about-contacts" href="index.php?route=information/contact">Контакты</a></li>
    </ul>
  </aside>
    <div id="content" class="col-sm-8 col-md-9"><?php echo $content_top; ?>
      <h1>КОНТАКТЫ</h1>
     

            <p>Мы будем рады ответить на Ваши вопросы 7 дней в неделю с 09:00 до 21:00 по московскому времени</p>
            <p>Тел: <?php echo $telephone; ?> (работает также Whats App, Viber, Telegram)</p>
            <p>E-mail: <a href="mailto:info@12s.ru">info@12s.ru</a></p>
           
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <legend>Напишите нам</legend>
  
      
              <?php if ($error_name) { 
                $error = 'error-class';

              }
              else { $error = '';
              }

              ?>
              
              <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="input <?php echo $error; ?>" placeholder="Ваше Имя" required />

           
        
             <?php if ($error_email) { 
                $error = 'error-class';
                
              }
              else { $error = '';
              }

              ?>
              <input type="email" name="email" value="<?php echo $email; ?>" id="input-email" class="input <?php echo $error; ?>" placeholder="E-mail" required/>

   
         
       
             <?php if ($error_enquiry) { 
                $error = 'error-class';
                 
              }
              else { $error = '';
              }

              ?>
              <textarea name="enquiry" rows="10" id="input-enquiry" class="input <?php echo $error; ?>" required ><?php echo $enquiry; ?></textarea>

        
          
          <?php echo $captcha; ?>
        </fieldset>
        <div class="buttons">
          <div class="pull-right">
            <input class="contact-button" type="submit" value="Отправить" />
          </div>
        </div>
      </form> 
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
