<?php echo $header; ?>
<div class="container-fluid">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row page-info">
  <aside id="column-left" class="col-sm-4 col-md-3">
    <ul class="list-unstyled sidebar__menu">
      <li><a data-id="6" href="index.php?route=information/support&information_id=6">Доставка & оплата</a></li>
      <li><a data-id="9" href="index.php?route=information/support&information_id=9">Обмен и возврат</a></li>
      <li><a data-id="10" href="index.php?route=information/support&information_id=10">Гарантия</a></li>
      <li><a data-id="11" href="index.php?route=information/support&information_id=11">Инструкции & Сервис</a></li>
      <li><a data-id="12" href="index.php?route=information/support&information_id=12">Сотрудничество</a></li>
    </ul>
  </aside>
    <div id="content" class=" col-sm-8 col-md-9"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php echo $description; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>