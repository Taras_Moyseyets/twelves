<?php echo $header; ?>
<div class="container-fluid">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row page-info">  
  <aside id="column-left" class="col-sm-4 col-md-3">
    <ul class="list-unstyled sidebar__menu">
      <li><a data-id="4" href="index.php?route=information/information&information_id=4">О нас</a></li>
      <li><a data-id="8" href="index.php?route=information/information&information_id=8">Оферта</a></li>
      <li><a href="index.php?route=information/contact">Контакты</a></li>
    </ul>
  </aside>
    <div id="content" class=" col-sm-8 col-md-9"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php echo $description; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>