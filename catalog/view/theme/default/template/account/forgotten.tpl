<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="window-forgot-your-password row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1 class="question-register h1-forgot-password">Восстановление пароля</h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <div class="form-group required">
            <label for="input-email"></label>
            <div class="input-forgot-password">
              <input type="text" name="email" value="<?php echo $email; ?>" placeholder="Email" id="input-email" class="input-regisAndlogin input-regisAndlogin-padding-login" />
              <div class="buttons clearfix forgot-your-password-buttons">
                <div class="pull-left"><a href="index.php?route=account/register" class="forgot-your-password">Отмена</a></div>
                <div class="pull-right">
                  <input type="submit" value="Восстановить пароль" class="send-button forgot-your-password-submit" />
                </div>
              </div>
            </div>
          </div>
        </fieldset>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>