<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="div-span-window-regisAndlogin personal-office">
        <span class="span-window-regisAndlogin span-window-regisAndlogin-office">Личный кабинет</span>
      </div>
      <aside class="aside-left-office col-lg-3 col-md-3 col-sm-4 col-xs-12">
        <ul class="list-unstyled">
          <li>
            <a href="index.php?route=account/edit" class="active">Личные данные</a>
          </li>
          <li>
            <a href="index.php?route=account/address">Мои адреса</a>
          </li>
          <li>
            <a href="index.php?route=account/order">Мои покупки</a>
          </li>
          <li>
            <a href="index.php?route=account/wishlist">Cписок желаний</a>
          </li>
          <li>
            <a href="index.php?route=account/logout">Выйти</a>
          </li>
        </ul>
      </aside>
      <?php if ($addresses) { ?>
      <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <?php print_r($addresses)?>
          <?php foreach ($addresses as $result) { ?>
          <tr>
            <td class="text-left"><?php echo preg_replace("/(<br \/>|\s)/", ", ", $result['address']); ?></td>
            <td class="text-right"><a href="<?php echo $result['update']; ?>" class="btn btn-info"><?php echo $button_edit; ?></a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="btn btn-danger"><?php echo $button_delete; ?></a></td>
          </tr>
          <?php } ?>
        </table>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
        <div class="pull-right"><a href="<?php echo $add; ?>" class="btn btn-primary"><?php echo $button_new_address; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>