<header class="header-cart">Корзина</header>
<?php if ($products || $vouchers) { ?>
<li>
  <div class="table table-striped">
    <?php foreach ($products as $product) { ?>
    <div class="row tr" id="<?=$product['product_id']?>">
    <input type="hidden" name="one_prod" value="<?=substr($product['price'],1)?>"/>
      <div class="td col-xs-12 col-sm-3 col-md-3 col-lg-2 text-center product-image"><?php if ($product['thumb']) { ?>
        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
        <?php } ?></div>
        <div class="td col-xs-12 col-sm-5 col-md-4 col-lg-5 text-left product-info">
            <a class="brend" href="#"> <?php echo $product['manufacturer']; ?></a>
            <a class="product-name" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            <span class="quantity">
                <button class="cart-button decrease">-</button>
                <input  type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control quantity_product" />
                <button type="submit" data-toggle="tooltip" class="cart-button increase">+</button>
            </span>       
        </div>
      <div class="td col-xs-12 col-sm-3 col-md-3 text-right product-option">
        <div class="color common-cart-color">
          <div class="color-item-header-cart">
            <?php foreach($product['parent'] as $color){$parent=strtolower($color['color']);
                switch($parent){
                    case "темно-коричневый":$parent="dark-brown"; break;
                    case "светло-коричневый":$parent="light-brown"; break;
                    case "серый":$parent="gray"; break;
                    case "черный":$parent="black"; break;
                    case "голубой":$parent="blue"; break;
                    case "красный":$parent="red"; break;
                    case "коричневый":$parent="brown"; break;
                    case "белый":$parent="white"; break;
                    case "бежевый":$parent="beige"; break;
                    case "синий":$parent="light-blue"; break;
                    case "темно-синий":$parent="dark-blue"; break;
                    case "светлый":$parent="light"; break;
                    case "стальной":$parent="steal"; break;
                }
                
                
            
            ?>
            <input  type="radio" name="color" id="header_<?=$parent?>"  hidden="" >
            <label cart_id="<?=$product['cart_id']?>" old_id="<?=$product['product_id']?>" product_id="<?=$color['product_id']?>" for="header_<?=$parent?>" class="<?=$parent?>"></label>              
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="td col-xs-12 col-sm-1 col-md-2 text-center product-price-block"><button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="cart-button-exit"><i class="fa fa-exit"></i></button>
      <div class="product-price"><?php echo $product['total']; ?></div>
      </div>
    </div class="tr">
    <?php } ?>
    <?php foreach ($vouchers as $voucher) { ?>
    <div class="tr">
      <div class="td text-center"></div>
      <div class="td text-left"><?php echo $voucher['description']; ?></div>
      <div class="td text-right">x&nbsp;1</div>
      <div class="td text-right"><?php echo $voucher['amount']; ?></div>
      <div class="td text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></div>
    </div>
    <?php } ?>
  </div>
</li>
<li>
  <div>
    <div class="table table-bordered">
      <?php foreach ($totals as $total) { ?>
      <div class="tr">
        <div class="td text-right product-total">Итого</div>
        <div class="td text-right pull-right"><?php echo $total['text']; ?></div>
      </div>
      <?php } ?>
    </div>
    <p class="text-center to-checkout">
            <!--  <a href="<?php echo $cart; ?>"><strong><i class="fa fa-shopping-cart"></i> <?php echo $text_cart; ?></strong></a>&nbsp;&nbsp;&nbsp;  -->
      <a href="<?php echo $checkout; ?>">Оформить заказ</a>
    </p>
    <p class="text-center continue">
      <a href="#">Продолжить покупки</a>
    </p>
  </div>
</li>
<?php } else { ?>
<li>
  <p class="text-center"><?php echo $text_empty; ?></p>
</li>
<?php } ?>