<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>

<script src="catalog/view/javascript/scrollbar-master/jquery.mCustomScrollbar.js" type="text/javascript"></script>
<link href="catalog/view/javascript/scrollbar-master/jquery.mCustomScrollbar.min.css" rel="stylesheet" media="screen" />

<script src="catalog/view/javascript/zoomsl-3.0.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery.bxslider/jquery.bxslider.min.js" type="text/javascript"></script>

<script src="catalog/view/javascript/fancyBox/jquery.fancybox.js" type="text/javascript"></script>
<script src="catalog/view/javascript/textchange.js" type="text/javascript"></script>

<script type="text/javascript" src="catalog/view/javascript/slick-slider/slick/slick.min.js"></script>
<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/account.js"></script>
<link href="catalog/view/javascript/fancyBox/jquery.fancybox.css" rel="stylesheet" media="screen" />

<link href="catalog/view/javascript/jquery.bxslider/jquery.bxslider.css" rel="stylesheet" media="screen" />


<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />

<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/jquery/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet" media="screen">
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/slick-slider/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/slick-slider/slick/slick-theme.css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/masonry/masonry.pkgd.min.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>

</head>
<body class="<?php echo $class; ?>">
<div class="shadow">

</div>
 <?php echo $search; ?>
<nav id="top">
<div class="product-footer">
  <div class="container-fluid">
  <!--   <?php echo $currency; ?> -->
    <!-- <?php echo $language; ?> -->
    <div class="hidden-xs nav pull-left phone-div header-font"><?php echo $telephone; ?></div>
      <div id="nav-toggle"><span></span></div>
        <ul class="list-unstyled header-menu ">
          <li><a href="index.php?route=product/category&path=60">Часы</a></li>
          <li><a href="index.php?route=information/information&information_id=4">О проекте</a></li>
          <li><a href="index.php?route=vblog/blog">Блог</a></li>
          <li><a href="index.php?route=information/support&information_id=6">Поддержка</a></li>
        </ul>
        <ul class="list-unstyled header-menu header-menu-mobile">
          <li><a href="index.php?route=product/category&path=60">Часы</a></li>
          <li><a href="index.php?route=information/information&information_id=4">О проекте</a></li>
          <li><a href="index.php?route=vblog/blog">Блог</a></li>
          <li><a href="index.php?route=information/support&information_id=6">Поддержка</a></li>
        </ul>
        <a href="tel:+74957411267"> 
        	<i class="fa fa-phone fa-phone-menu-mobile" aria-hidden="true"></i> 
        </a>
    <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
    </div>
    <div id="top-links" class="nav pull-right">
      <ul class="list-inline header-icon-all">       
       <li> <a href="#" class="header-links" id="search-button"><i class="icon icon-searsh"></i></a></li>
        <li class="hidden-xs"><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?> " class="header-links"><i class="icon icon-star"></i> <span class=" header-icon-span"><?php $text_wishlist = preg_replace('~[^0-9]+~','',$text_wishlist); if ($text_wishlist>0) {echo $text_wishlist;} ?></span></a></li>

         <li class="hidden-xs dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle header-links" data-toggle="dropdown"><i class="icon icon-account"></i> <span class="hidden-xs hidden-sm hidden-md"></span> <span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </li>
        <li><?php echo $cart; ?></li>
      </ul>
    </div>
  </div>
  </div>
</nav>
<div class="wrapper">
<!-- <?php if ($categories) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>
<?php } ?> -->
