<?php echo $header; ?>

<?php echo $content_top; ?>

<div class="product-footer">
<div class="container-fluid">

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"></div>
    <?php echo $column_right; ?></div>
</div>
</div>

<div class="container-fluid">
    <div class="row">
    <?php
        foreach($category as $cat){?>
        <div class="cat-block col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <a href="<?=$cat['href'];?>">
                <img src="image/<?=$cat['image'];?>" alt="">
                <span><?=$cat['name'];?>
                    <span class="line"></span>
                </span>
            </a>
        </div>
        <?php } ?>
  
    </div>
</div>

<?php echo $content_bottom; ?>

<div class="news-block container-fluid">
    <div class="row">
        <div class="cover-news col-lg-6 col-md-6 col-sm-12 col-xs-12">
            
                <div class="cover-news-img">
                    <a href="<?=$vblog[3]['href']?>">
                        <img src="<?=$vblog[3]['image']?>" alt="">
                        <span class="news-span"><?=$vblog[3]['title']?></span>
                        <?php if(!empty($vblog[3]['text'])){?>
                            <span class="news-short-content">
                              <?=substr($vblog[3]['text'],0,500)?>
                            </span>
                        <?php } ?>
                    </a>
                </div>
                <div class="cover-news-img">
                    <a href="<?=$vblog[1]['href']?>">
                        <img src="<?=$vblog[1]['image']?>" alt="">
                         <span class="news-span color-gray"><?=$vblog[1]['title']?></span>
                    </a>
                </div>
           
        </div>
        <div class="cover-news cover-news-right col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="cover-news-img">
                <a href="<?=$vblog[2]['href']?>">
                    <img src="<?=$vblog[2]['image']?>" alt="">
                    <span class="news-span color-gray news-span-right"><?=$vblog[2]['title']?></span>
                    <?php if(!empty($vblog[2]['text'])){?>
                    <span class="news-short-content">
                      <?=substr($vblog[2]['text'],0,500)?>
                    </span>
                    <?php } ?>
                </a>
            </div>
            <div class="cover-news-img align-right ">
                <a href="<?=$vblog[0]['href']?>">
                    <img src="<?=$vblog[0]['image']?>" alt="">
                    <span class="news-span color-gray news-span-left"><?=$vblog[0]['title']?></span>                    
                </a>
            </div>
        </div>
    </div>
	
    <a href="<?=$vblog_link?>" class="all-article">все статьи</a>
</div>
<div class="container-fluid">
    <div class="row">
        <section class="news-new">
            <div class="col-md-6">
                <div class="news-new-text">
                    <div class="header">new</div>
                    <p class="header-news"><?php echo $name;?>
                    </p>
                    <a href="<?php echo $href;?>" class="more my-btn"><?php echo $button_text;?></a>
                </div>
            </div>      
         
            <div class="col-md-6">
                <img src="<?php echo $thumb;?>" alt="" class="news-image">
            </div>
        </section>
    </div>
</div>
<section class="instra">
    <div class="container-fluid">
        <div class="row my-row"> 
           <?php for($i=0;$i<6;$i++){?>
             <a href="<?=$instagram[$i]['link']?>" target="_blank" class="mass-class">
                <img src="<?=$instagram[$i]['image']?>" alt="" class="instra-link">
            </a>
            <?php } ?>
        </div>
        <div class="row my-row"> 
              <?php for($i=6;$i<12;$i++){?>
            <a href="<?=$instagram[$i]['link']?>" target="_blank" class="mass-class">
                <img src="<?=$instagram[$i]['image']?>" alt="" class="instra-link">
            </a>
            <?php } ?>
        </div>
    </div>
</section>
<section class="about-brands">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md12">
                <header>Лучшие бренды часов в нашем интернет магазине</header>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
               <p>Hublot – бренд-создатель знаменитой модели «Big Bang», выпущенной в 2005 году и имевшей головокружительный успех. Эти часы получили множество международных наград, в том числе на Гран-При «Женевского Конкурса производителей часов». Официальные лица этого бренда – футболист Диего Марадона, бегун-рекордсмен Усейн Болт и участники футбольного клуба Манчестер Юнайтед.</p>
               <p>Patek Philippe – классика швейцарских часов, символ точности, надежности и стабильности. Возможно, именно поэтому этот часовой бренд так любят политики. Среди владельцев часов от Patek Philippe Николя Саркози, римские папы Pius IX и Leo XIII, королевские семьи Дании и Италии, султан Египта Хусейн Ка. В прошлом их носили такие выдающиеся личности, как Мария Кюри, Альберт Эйнштейн, Уолт Дисней, Лев Толстой и Петр Чайковский.</p>
            </div>
        </div>
    </div>
</section>
       </div>
    </div>
<?php echo $footer; ?>