<?php echo $header; ?>

<section class="blog-slider blog-container">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="b-slider">
					<img src="img/slider1.png" alt="">
					<img src="img/slider2.png" alt="">
					<img src="img/slider3.png" alt="">
					<img src="img/slider4.png" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
<section class="blog-container">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<div class="blog-items">
					<header>Водонепроницаемые часы и 4 уровня защиты</header>
					<img src="img/news1.jpg" alt="">
					<p>На сегодняшний день водонепроницаемые часы являются уже не прихотью, а отличным решением в таких вопросах, как удобство и новые возможности. Ведь, не очень комфортно каждый раз снимать с руки этот аксессуар перед мытьем рук, либо же при неожиданно начавшемся дожде. Согласитесь, не лучшим выходом является и постоянные походы в мастерскую, если вдруг по забывчивости вы оказались с часами под душем или в бассейне.</p>
					<div class="footer-social">
			          <a href="#"><i class="icon-footer icon-facebook"></i></a>
			          <a href="#"><i class="icon-footer icon-vk"></i></a>
			          <a href="#"><i class="icon-footer icon-odn"></i></a>
			          <a href="#"><i class="icon-footer icon-google"></i></a>
			          <a href="#"><i class="icon-footer icon-yandex"></i></a>
			        </div>
					<a href="#" class="all-article">читать дальше</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="snews">
					<img src="img/snews1.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
</section>


<?php echo $footer; ?>