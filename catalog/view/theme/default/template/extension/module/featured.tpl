<div class="container-fluid">
<h3>Новая колекция</h3>
<div class="row">
<div class="cover-carousel">
<div class="carousel">

  <?php foreach ($products as $product) { ?>
  <div class="product-layout col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="product-thumb transition">
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
      <div class="caption">
      <p class="brend"><?php print_r($product['manufacturer']); ?></p>
        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
        <!-- <p><?php echo $product['description']; ?></p> -->
<!--         <?php if ($product['rating']) { ?>
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?> -->
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
      </div>
      <div class="button-group">
<!--         <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="pr-icon icon-pr-star"></i></button> -->
        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="addto-cart addto-cart-two">В корзину</button>
      </div>
    </div>
  </div>
  <?php } ?>

</div>
  <div class="arrow arrow-left"></div>
  <div class="arrow arrow-right"></div>
</div>
</div>
</div>
<script type="text/javascript"><!--
var owl = $(".carousel");
  
  owl.owlCarousel({
  items: 4,
  itemsDesktop: [1199, 4],
  itemsDesktopSmall: [979, 3],
  itemsTablet: [768, 2],
  itemsMobile : [650, 2],
  pagination: false
});
  $(".cover-carousel .arrow-right").click(function(){
    owl.trigger("owl.next");
  });
  $(".cover-carousel .arrow-left").click(function(){
    owl.trigger("owl.prev");
  });
--></script>