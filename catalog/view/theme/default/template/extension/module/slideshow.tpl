<div class="main-slider">
<div id="slideshow<?php echo $module; ?>" class="owl-carousel" style="opacity: 1;">
  <?php foreach ($banners as $banner) { ?>
  <div class="item">
    <?php if ($banner['link']) { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
       <a href="<?php echo $banner['link']; ?>" class="main-more">
        <?=$banner['button_title']?></a>
         <div class="main-header-slider">
  <?php echo $banner['title']; ?>
  <span><?=$banner['description']?></span>
  </div>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
      <a href="#" class="main-more"><?=$banner['button_title']?></a>
        <div class="main-header-slider">
      <?php echo $banner['title']; ?>
      <span><?=$banner['description']?></span>
  </div>
    <?php } ?>
  </div>
  <?php } ?>
</div>
<div class="arrow arrow-left"></div>
<div class="arrow arrow-right"></div>
</div>
<script type="text/javascript"><!--
$('#slideshow<?php echo $module; ?>').owlCarousel({
	items: 1,
	autoPlay: true,
	singleItem: true,
	// navigation: true,
	// navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
	pagination: true
});

  $(".main-slider .arrow-right").click(function(){
    $('#slideshow<?php echo $module; ?>').trigger("owl.next");
  });
  $(".main-slider .arrow-left").click(function(){
    $('#slideshow<?php echo $module; ?>').trigger("owl.prev");
  });
--></script>