<div class="filter">
  <ul class="sidebar-filter">
    <?php foreach ($filter_groups as $filter_group) { ?>
    <li class="list-unstyled sidebar-filter-item "><span><?php echo $filter_group['name']; ?></span> <i class="filter-arrow"></i>
  
      <ul>
       <?php foreach ($filter_group['filter'] as $filter) { ?>
        <?php if (in_array($filter['filter_id'], $filter_category)) { ?>
        <li class="list-unstyled">
          <input class="input" type="checkbox" id="<?php echo $filter['name']; ?>" name="filter[]" class="input" value="<?php echo $filter['filter_id']; ?>" checked="checked"  >
          <label for="<?php echo $filter['name']; ?>" class="<?php echo $filter['name']; ?>" ><?php echo $filter['name']; ?>     </label>
        </li>
         <?php } else { ?>
            <li class="list-unstyled">
          <input type="checkbox" id="<?php echo $filter['name']; ?>" name="filter[]" value="<?php echo $filter['filter_id']; ?>"   >
          <label for="<?php echo $filter['name']; ?>" class="<?php echo $filter['name']; ?>"><?php echo $filter['name']; ?>     </label>
        </li>
        <?php }} ?>
      </ul>
    </li>
    <?php } ?>

  </ul>
</div>

<script type="text/javascript"><!--
$('input').on('change', function() {
	filter = [];
        console.log("Q");
	$('input[name^=\'filter\']:checked').each(function(element) {
		filter.push(this.value);
	});

	location = '<?php echo $action; ?>&filter=' + filter.join(',');
});
//--></script>
