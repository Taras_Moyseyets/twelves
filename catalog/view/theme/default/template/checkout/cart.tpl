<?php echo $header; ?>
<div class="product-footer nx-cart-cover">
<div class="container-fluid">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($attention) { ?>
  <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
<!--       <h1><?php echo $heading_title; ?>
        <?php if ($weight) { ?>
        &nbsp;(<?php echo $weight; ?>)
        <?php } ?>
      </h1> -->
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

          <div class="table table-striped">
            <div>
              <?php foreach ($products as $product) { ?>
              <div class="row tr">
                <div class="td col-xs-12 col-sm-3 col-md-2 text-center"><?php if ($product['thumb']) { ?>
                  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                  <?php } ?></div>
                <div class="td col-xs-12 col-sm-4 col-md-4 text-left  product-info"> 
                <a class="brend" href="#"><?php echo $product['manufacturer']; ?></a>
          			<a class="product-name" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                      <div class="color">
     <div class="color-item-cart">
                <?php foreach($product['parent'] as $color){$parent=strtolower($color['color']);?>
                <input  type="radio" name="color" id="<?=$parent?>"  hidden="" >
                <label cart_id="<?=$product['cart_id']?>"old_id="<?=$product['product_id']?>" product_id="<?=$color['product_id']?>" for="<?=$parent?>" class="<?=$parent?>"></label>              
                <?php } ?>
                
                
          
              </div>
			            </div>
                 <!--  <?php if (!$product['stock']) { ?>
                  <span class="text-danger">***</span>
                  <?php } ?>
                  <?php if ($product['option']) { ?>
                  <?php foreach ($product['option'] as $option) { ?>
                  <br />
                  <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                  <?php } ?>
                  <?php } ?>
                  <?php if ($product['reward']) { ?>
                  <br />
                  <small><?php echo $product['reward']; ?></small>
                  <?php } ?>
                  <?php if ($product['recurring']) { ?>
                  <br />
                  <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                  <?php } ?> -->
                  </div>
                <div class="td text-left col-xs-12 col-sm-2 col-md-2"> 
                	<span class="quantity">
                		<button class="cart-button">-</button>
                		<?php echo $product['quantity']; ?>
                		<button class="cart-button">+</button>
            		</span>
            	</div>

<!-- 	          рефреш ы всы дыла      <div class="input-group btn-block" style="max-width: 200px;">
	                    <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control" />
	                    <span class="input-group-btn">
	                   <!--  <button type="submit" data-toggle="tooltip" title="<?php echo $button_update; ?>" class="btn btn-primary"><i class="fa fa-refresh"></i></button> 
	                    </span>
	                </div> -->
         
               
                <div class="td text-right col-xs-12 col-sm-3 col-md-4 product-price-block">
                <button class="star-button cart-star-button" type="button" data-toggle="tooltip" title="" onclick="wishlist.add('<?php echo $product['cart_id']; ?>');" data-original-title="Add to Wish List"><i class="pr2-icon icon-pr2-star"></i></button>
                           <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="cart-button-exit" onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><i class="fa fa-exit"></i></button>
                 <div class="product-price"><?php echo $product['total']; ?></div>
                </div>
              </div>
              <?php } ?>
             <!--  <?php foreach ($vouchers as $voucher) { ?>
              <tr class="row">
                <td></td>
                <td class="text-left col-md-3"><?php echo $voucher['description']; ?></td>
                <td class="text-left"></td>
                <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">
                    <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />
                    <span class="input-group-btn">
                    <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="voucher.remove('<?php echo $voucher['key']; ?>');"><i class="fa fa-times-circle"></i></button>
                    </span></div></td>
                <td class="text-right"><?php echo $voucher['amount']; ?></td>
                <td class="text-right"><?php echo $voucher['amount']; ?></td>
              </tr>
              <?php } ?> -->
            </div>
          </div>
      
      </form>
      <?php if ($modules) { ?>
      <h2><?php echo $text_next; ?></h2>
      <p><?php echo $text_next_choice; ?></p>
      <div class="panel-group" id="accordion">
        <?php foreach ($modules as $module) { ?>
        <?php echo $module; ?>
        <?php } ?>
      </div>
      <?php } ?>
      <br />

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</div>
<section class="product-footer nx-cart-cover">
	
	<div class="container-fluid">
	<div class="col-md-8">
	  <div class="row">
	      

	<!-- RELATED -->
	<!-- RELATED -->
	<!-- RELATED -->
	<!-- RELATED -->
	<!-- RELATED -->
	<!-- RELATED -->


	      <?php if ($products) { ?>
	      <ul class="nav nav-tabs">
	        <li class="active"><a href="#tab-related" data-toggle="tab" aria-expanded="true">Вам может понравится </a></li>
	        <li class=""><a href="#tab-latest" data-toggle="tab" aria-expanded="false">ВЫ ПРОСМОТРИВАЛИ:</a></li>
	      </ul>
	      <div class="tab-content">
	      <div class="tab-pane active" id="tab-related">
		  <div class="cover-carousel">
	<div class="carousel">

	  <?php foreach ($products_releted as $product) { ?>
	  <div class="product-layout">
	    <div class="product-thumb transition">
	      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
	      <div class="caption">
	      <div class="color">
	              <div class="color-item">
	                
	                <input type="radio" name="color" id="green"  hidden="" >
	                <label for="green" class="green"></label>              

	                <input type="radio" name="color" id="red"  hidden="">
	                <label for="red" class="red"></label>              

	                <input type="radio" name="color" id="yellow"  hidden="">
	                <label for="yellow" class="yellow"></label>              

	                <input type="radio" name="color" id="blue"  hidden="">
	                <label for="blue" class="blue"></label>
	              </div>
	            </div>
	      <p class="brend">><?php echo $product['manufacturer']; ?></p>
	        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
	        <!-- <p><?php echo $product['description']; ?></p> -->
	<!--         <?php if ($product['rating']) { ?>
	        <div class="rating">
	          <?php for ($i = 1; $i <= 5; $i++) { ?>
	          <?php if ($product['rating'] < $i) { ?>
	          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	          <?php } else { ?>
	          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
	          <?php } ?>
	          <?php } ?>
	        </div>
	        <?php } ?> -->
	      
	        <p class="price">
	         <?php echo $product['price']; ?>
	        </p>
	      </div>
	      <div class="button-group">
	<!--         <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="pr-icon icon-pr-star"></i></button> -->
	        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="addto-cart">В корзину</button>
	      </div>
	    </div>
	  </div>
	  <?php } ?>

	</div>
	  <div class="arrow arrow-left"></div>
	  <div class="arrow arrow-right"></div>
	</div>
	<script type="text/javascript"><!--
	var owl = $(".carousel");
	  
	  owl.owlCarousel({
	  items: 3,
	  itemsDesktop: [1199, 2],
	  itemsDesktopSmall: [979, 2],
	  itemsTablet: [768, 1],
	  itemsMobile : [650, 1],
	  pagination: false
	});
	  $(".arrow-right").click(function(){
	    owl.trigger("owl.next");
	  });
	  $(".arrow-left").click(function(){
	    owl.trigger("owl.prev");
	  });
	--></script>

</div>
	          <div class="tab-pane" id="tab-latest">
  	<div class="cover-carousel2">
	<div class="carousel2">

	  <?php foreach ($watched_data as $product) { ?>
	  <div class="product-layout">
	    <div class="product-thumb transition">
	      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
	      <div class="caption">
	      <div class="color">
	              <div class="color-item">
	                
	                <input type="radio" name="color" id="green"  hidden="" >
	                <label for="green" class="green"></label>              

	                <input type="radio" name="color" id="red"  hidden="">
	                <label for="red" class="red"></label>              

	                <input type="radio" name="color" id="yellow"  hidden="">
	                <label for="yellow" class="yellow"></label>              

	                <input type="radio" name="color" id="blue"  hidden="">
	                <label for="blue" class="blue"></label>
	              </div>
	            </div>
	      <p class="brend">TUDOR HERITAGE</p>
	        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
	        <!-- <p><?php echo $product['description']; ?></p> -->
	<!--         <?php if ($product['rating']) { ?>
	        <div class="rating">
	          <?php for ($i = 1; $i <= 5; $i++) { ?>
	          <?php if ($product['rating'] < $i) { ?>
	          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	          <?php } else { ?>
	          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
	          <?php } ?>
	          <?php } ?>
	        </div>
	        <?php } ?> -->
	      
	        <p class="price">
	        <?php echo $product['price']; ?>
	        </p>
	      </div>
	      <div class="button-group">
	<!--         <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="pr-icon icon-pr-star"></i></button> -->
	        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="addto-cart">В корзину</button>
	      </div>
	    </div>
	  </div>
	  <?php } ?>

	</div>
	  <div class="arrow arrow-left2"></div>
	  <div class="arrow arrow-right2"></div>
	</div>
	<script type="text/javascript"><!--
	var owl2 = $(".carousel2");
	  
	  owl2.owlCarousel({
	  items: 3,
	  itemsDesktop: [1199, 2],
	  itemsDesktopSmall: [979, 2],
	  itemsTablet: [768, 1],
	  itemsMobile : [650, 1],
	  pagination: false
	});
	  $(".arrow-right2").click(function(){
	    owl.trigger("owl2.next");
	  });
	  $(".arrow-left2").click(function(){
	    owl.trigger("owl2.prev");
	  });
	--></script>
<?php } ?>

    </div>
	  </div>
	  </div>
	</div>
	<div class="col-md-4">
	      <div class="row">
	       
	          <div class="table table-bordered continue-cart-ornot">
	            <?php foreach ($totals as $total) { ?>
	            <div>
	              <div class="text-right pull-left product-total">Итого:</div>
	              <div class="text-right pull-right"><?php echo $total['text']; ?></div>
	            </div>
	            <?php } ?>
	          </div>
	        </div>
	      <div class="buttons clearfix">
	        <div class="cover-cart-button to-checkout"><a href="<?php echo $checkout; ?>" class=""><?php echo $button_checkout; ?></a></div>
	        <div class="cover-cart-button continue"><a href="<?php echo $continue; ?>" class=""><?php echo $button_shopping; ?></a></div>
	      </div>
</div>
</div>

</section>
<?php echo $footer; ?>
