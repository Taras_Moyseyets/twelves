<?php echo $header; ?>
<script src="catalog/view/javascript/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
<script src="catalog/view/javascript/product.js" type="text/javascript"></script>
<div class="container-fluid">
<!--   <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul> -->
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content " class="<?php echo $class; ?> nx-product"><?php echo $content_top; ?>
      <div class="row">
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-7 col-md-7 col-lg-7'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
          <?php if ($thumb || $images) { ?>
          <ul class="thumbnails">
            <?php if ($thumb) { ?>
            <li class="main-photo main-photo-custom-class-support">
                <a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                    <img src="<?php echo $thumb; ?>" data-large="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                </a>
                <span class="discount">
                <?php if($special_discount){ ?>
                    скидка <span><?=$special_discount?>%</span>
                <?php } elseif($date_available){ ?>
                    Ожидается с <?=$date_available?>
                <?php } elseif($new_product){ ?>
                    Новинка
                <?php } ?>
                </span>
				<?php if($print_image){ ?>
                <span class="print"><i class="print-icon"></i><span>распечатать в оригинальном размере</span></span>
				<?php } ?>
			</li>
            <?php } ?>
             
            <?php if ($images) { ?>
              <span class="cover-vs">
              <span class="verical-slider">              
              <?php foreach ($images as $image) { ?>
              <li class="image-additional"><a class="" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
              <?php } ?>

                <li class="image-additional">
                    <a class="" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                    <img src="<?php echo $thumb; ?>" data-large="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                    </a>
                </li>

                
              </span>
              <a class="vertical-tools up" id="up"></a>
              <a class="vertical-tools down " id="down"></a>
            </span>

            <?php } ?>

          </ul>
          <?php } ?>


        </div>
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-5 col-md-4 col-lg-4'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?> my-sidebar">

          <ul class="list-unstyled">
            <?php if ($manufacturer) { ?>
           <span class="cover-brend">
               <li>
                 <a class="brend" href="<?php echo $manufacturers; ?>">   <?php echo $manufacturer; ?></a>
             </li>
             <?php } ?>
            <h1><?php echo $heading_title; ?></h1>
            </span>
            <img class="img-brend" src="<?=$manufacturer_image?>" alt="">
            <!--
            <li><?php echo $text_model; ?> <?php echo $model; ?></li>
            <?php if ($reward) { ?>
            <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
            <?php } ?>
            -->
            <div class="color">
                <div class="color-title">Цвет:</div>
                <div class="color-items">
                <?php
                foreach($parent as $item){
                    $item['color']=strtolower($item['color']);
                ?>
                <input <?php if($_GET['product_id']==$item['product_id']) echo "checked"; ?>  type="radio" name="<?php echo $item['color'];?>" id="<?php echo $item['color'];?>"  hidden="" >
                  <label  product_id="<?=$item['product_id']?>"  for="<?=$item['color']?>" class="<?=$item['color']?>"></label>            
                <?php } ?>              
              </div> 
            </div>
            <li class="stock"><?php echo $stock; ?></li>
          </ul>
          
          <?php if ($price) { ?>
          <ul class="list-unstyled price-block">
            <?php if (!$special) { ?>
            <li>
              <h2><?php echo $price; ?></h2>
            </li>
            <?php } else { ?>
            <li><span class="old-price" ><?php echo $price; ?></span></li>
            <li>
              <h2 class="new-price-custom-style"><?php echo $special; ?></h2>
            </li>
            <?php } ?>
           <!--  <?php if ($tax) { ?>
            <li><?php echo $text_tax; ?> <?php echo $tax; ?></li>
            <?php } ?>
            <?php if ($points) { ?>
            <li><?php echo $text_points; ?> <?php echo $points; ?></li>
            <?php } ?>
            <?php if ($discounts) { ?>
            <li>
              <hr>
            </li> -->
<!--             <?php foreach ($discounts as $discount) { ?>
            <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
            <?php } ?> -->
            <!-- <?php } ?> -->
          </ul>
          <?php } ?>
    <!--       <div id="product">
            <?php if ($options) { ?>
            <hr>
            <h3><?php echo $text_option; ?></h3>
            <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'select') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio">
                  <label>
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php if ($option_value['image']) { ?>
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                    <?php } ?>                    
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php if ($option_value['image']) { ?>
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                    <?php } ?>
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'text') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group date">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group datetime">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php if ($recurrings) { ?>
            <hr>
            <h3><?php echo $text_payment_recurring; ?></h3>
            <div class="form-group required">
              <select name="recurring_id" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($recurrings as $recurring) { ?>
                <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                <?php } ?>
              </select>
              <div class="help-block" id="recurring-description"></div>
            </div>
            <?php } ?>

            <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
            <?php } ?>
          </div> -->

          <div class="form-group form-group-custom-style">
             <!--  <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
              <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" /> -->
           <!--    <br /> -->
               <a product_id="<?=$product_id?>" href="#bay-one-click-first-window" real="group" id="button-cart1" data-loading-text="<?php echo $text_loading; ?>" class="addto-cart one-click one-clicks">Купить в 1 клик</a>
              <a id="button-cart" data-loading-text="<?php echo $text_loading; ?>"  onclick="cart.add(<?=$product_id?>)" class="addto-cart addto-cart-real addto-cart-third"><?php echo $button_cart; ?></a>
            </div>
            <div class="btn-group btn-group-custom-style">
              <button class="star-button" type="button" data-toggle="tooltip" class=""  onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="pr2-icon icon-pr2-star icon-pr2-star-custom-style <?php echo $wish_class; ?>"></i></button>
              <button class="share-button"><i class="pr2-icon icon-pr2-share icon-pr2-share-custom-style"></i></button>
            </div>

        <ul class="nav nav-tabs">
            <?php if ($attribute_groups) { ?>
            <li class="active"><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
            <?php } ?>
            <li ><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
            <?php if ($review_status) { ?>
            <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
            <?php } ?>
          </ul>
          <div class="tab-content">
            <div class="tab-pane " id="tab-description"><?php echo $description; ?></div>
            <?php if ($attribute_groups) { ?>
            <div class="tab-pane active" id="tab-specification">
              <div class="table table-bordered products-options">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
               
                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                  <div class="tr">
                    <div class="td"><?php echo $attribute['name']; ?>:</div>
                    <div class="td"><?php echo $attribute['text']; ?></div>
                  </div>
                  <?php } ?>
            
                <?php } ?>
              </div>
            </div>
            <?php } ?>
   
            <div class="tab-pane" id="tab-review">
       
        <?php echo $category_delivery; ?>
            </div>
  
          </div>
          <?php if ($review_status) { ?>
          <div class="rating">
            <p>
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($rating < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } ?>
              <?php } ?>
              <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a></p>
            <hr>

          </div>
          <?php } ?>



        </div>
      </div>


      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>





    <div id="bay-one-click-first-window">
     <span class="window-text">Спасибо за размещение заказа и т.д.</span>
      <input name="first_name_first"class="input" type="text" placeholder="Имя">
      <input name="first_telephone"class="input" type="text" placeholder="Телефон">
      <input name="first_mail"class="input" type="text" placeholder="e-mail">
  
      <a href="#bay-one-click-two-window" real="group" class="one-click one-clicks">Офромить заказ</a>
    </div>




    <div id="bay-one-click-two-window">
      <form action="#">
      <input class="input" placeholder="Телефон" value="<?php if(isset($user['telephone'])) echo $user['telephone']; ?>" type="text" name="telephone" "placeholder="Номер телефона" title="Формат:+7 (000) 000-00-00" pattern="+7 ([0-9]{3}) [0-9]{3}-[0-9]{2}-[0-9]{2}" required="">
      <input list="country" value="<?php if(isset($user['country'])) echo $user['country']; ?>" class="input" type="text" name="country" placeholder="Страна">
     
        
        <datalist id="country">
        <?php
         $headerOptions = array(
        'http' => array(
            'method' => "GET",
            'header' => "Accept-language: en\r\n" . // Вероятно этот параметр ни на что не влияет
            "Cookie: remixlang=$lang\r\n"
           )
             );
              $streamContext = stream_context_create($headerOptions);
        $countries=json_decode(file_get_contents("https://api.vk.com/api.php?oauth=1&method=database.getCountries&v=5.5",false,$streamContext),1);
    
        foreach($countries['response']['items'] as $countrie){ ?>
                <option value="<?=$countrie['title']?>" >
                
         <?php }
                 $countries=json_decode(file_get_contents("https://api.vk.com/api.php?oauth=1&method=database.getCountries&v=5.5&need_all=1",false,$streamContext),1);
   
        foreach($countries['response']['items'] as $countrie){ ?>
                <option value="<?=$countrie['title']?>" >
                
         <?php } ?>
          ?>
       
        </datalist>
      <input class="input" type="text" name="city" placeholder="Город" id="">
      <input  value="<?php if(isset($user['address_1'])) echo $user['address_1']; ?>" class="input pull-left widthfifty" type="text" name="address_1" placeholder="Улица">
      <input class="input pull-left widthquoter" type="text" name="house" placeholder="Дом">
      <input class="input pull-right widthquoter" type="text" name="flat" placeholder="Квартира">
      <input class="input" type="text" placeholder="Имя" name="first_name" value="<?php if(isset($user['name'])){ $name=explode(" ",$user['name']); echo $name[0]; } ?>">
      <input class="input" type="text" placeholder="Фамилие" name="last_name" value="<?php if(isset($user['name'])) echo $name[1]; ?>">
      <input class="input" type="text" placeholder="e-mail" name="email" value="<?php if(isset($user['mail'])) echo $user['mail']; ?>">
      <textarea class="input" name="" id="" cols="30" rows="10" name="comment" placeholder="Комментарий к заказу"></textarea>
      <input class="input" type="text" placeholder="промокод">
      <select name="payment" class="input" name="" id="">
        
        <option value="paypal">PayPal</option>
        <option value="money">Наличными</option>
        <option value="yandex">Yandex money</option>
      </select> 
      <input type="submit" class="pay" value="Офромить заказ">
        <div class="for_pp" style="display:none;"> </div>
      </form>



    </div>


<!-- social-block  -->
    <div class="social-block">

      <a href="#"><i class="icon-share-back facebook"></i></a>
      <a href="#"><i class="icon-share-back vk"></i></a>
      <a href="#"><i class="icon-share-back odn"></i></a>
      <a href="#"><i class="icon-share-back google"></i></a>
      <a href="#"><i class="icon-share-back yandex"></i></a>
                            <!-- AddThis Button BEGIN -->
<!--             <div class="addthis_toolbox addthis_default_style" data-url="<?php echo $share; ?>"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script> -->
            <!-- AddThis Button END -->
    </div>
<!-- social-block end -->
<script>
$("[name=first_telephone]").mask("+7(999) 999-9999");
$("[name=telephone]").mask("+7(999) 999-9999");
</script>

<!-- product added -->
    <div class="product-added">
     <div class="exit2"></div>
      <div class="add-product-class">ТОВАР ДОБАВЛЕН В КОРЗИНУ</div>
      <img src="" alt="">
      <ul class="list-unstyled">
        <li class="brend"></li>
        <h1></h1>
      </ul>
      <p class="text-center to-checkout">
                <!--  <a href="https://nexll.com.ua/twelves/index.php?route=checkout/cart"><strong><i class="fa fa-shopping-cart"></i> View Cart</strong></a>&nbsp;&nbsp;&nbsp;  -->
          <a href="https://nexll.com.ua/twelves/index.php?route=ncheck/check">Оформление заказа</a>
        </p>
        <p class="text-center continue">
          <a href="#">Выбрать еще одни часы</a>
        </p>
    </div>
<!-- product added end-->


<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
  $.ajax({
    url: 'index.php?route=product/product/getRecurringDescription',
    type: 'post',
    data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
    dataType: 'json',
    beforeSend: function() {
      $('#recurring-description').html('');
    },
    success: function(json) {
      $('.alert, .text-danger').remove();

      if (json['success']) {
        $('#recurring-description').html(json['success']);
      }
    }
  });
});
//--></script>
<script type="text/javascript"><!--

$('.share-batton').click(function(){
  $('.back').show();
  $('.social-block').show();
    $('.exit').show();
});


$('.addto-cart-real').click(function(){
  $('.back').show();
   $('.product-added').show  ();
  var img = $('.main-photo img').attr('src');
  var brand = $('.my-sidebar .brend').text();
  var pr = $('.my-sidebar h1').text();
  $('.product-added img').attr('src', img);
  $('.product-added .brend').text(brand);
  $('.product-added h1').text(pr);
  $('.exit').hide();
});

$('.one-clicks').fancybox({
  openEffect  : 'none',
  closeEffect : 'none',
});




$('.addto-cart').on('click', function() {
  $.ajax({
    url: 'index.php?route=checkout/cart/add',
    type: 'post',
    data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
    dataType: 'json',
    beforeSend: function() {
      $('.addto-cart').button('loading');
    },
    complete: function() {
      $('.addto-cart').button('reset');
    },
    success: function(json) {
      $('.alert, .text-danger').remove();
      $('.form-group').removeClass('has-error');

      if (json['error']) {
        if (json['error']['option']) {
          for (i in json['error']['option']) {
            var element = $('#input-option' + i.replace('_', '-'));

            if (element.parent().hasClass('input-group')) {
              element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
            } else {
              element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
            }
          }
        }

        if (json['error']['recurring']) {
          $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
        }

        // Highlight any found errors
        $('.text-danger').parent().addClass('has-error');
      }

      if (json['success']) {
        $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

        $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');

        $('html, body').animate({ scrollTop: 0 }, 'slow');

        $('#cart > ul').load('index.php?route=common/cart/info ul li');
      }
    },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
  });
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

$('.datetime').datetimepicker({
  pickDate: true,
  pickTime: true
});

$('.time').datetimepicker({
  pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
  var node = this;

  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=tool/upload',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $('.text-danger').remove();

          if (json['error']) {
            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
          }

          if (json['success']) {
            alert(json['success']);

            $(node).parent().find('input').val(json['code']);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
  $.ajax({
    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
    type: 'post',
    dataType: 'json',
    data: $("#form-review").serialize(),
    beforeSend: function() {
      $('#button-review').button('loading');
    },
    complete: function() {
      $('#button-review').button('reset');
    },
    success: function(json) {
      $('.alert-success, .alert-danger').remove();

      if (json['error']) {
        $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }

      if (json['success']) {
        $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

        $('input[name=\'name\']').val('');
        $('textarea[name=\'text\']').val('');
        $('input[name=\'rating\']:checked').prop('checked', false);
      }
    }
  });
});

// $(document).ready(function() {
//   $('.thumbnails').magnificPopup({
//     type:'image',
//     delegate: 'a',
//     gallery: {
//       enabled:true
//     }
//   });
// });
//--></script>
<section class="product-footer">
<div class="container-fluid">
  <div class="row">
      





<!-- RELATED -->
<!-- RELATED -->
<!-- RELATED -->
<!-- RELATED -->
<!-- RELATED -->
<!-- RELATED -->







 
      <ul class="nav nav-tabs">
       <?php if ($products) { ?><li class="active"><a href="#tab-related" data-toggle="tab" aria-expanded="true">ВАС ТАКЖЕ МОЖЕТ ЗАИНТЕРЕСОВАТЬ:</a></li> <?php } ?>
        <?php if (isset($watched_data)) { ?><li <?php if (!$products) echo 'class="active"'?>><a href="#tab-latest" data-toggle="tab" aria-expanded="false" >ВЫ ПРОСМАТРИВАЛИ:</a></li> <?php }?>
      </ul>
      <div class="tab-content">
      <div class="tab-pane active" id="tab-related">
<?php if ($products) { ?>
<div class="cover-carousel">
<div class="carousel">

<?php foreach ($products as $product) { ?>
  <div class="product-layout">
    <div class="product-thumb transition">
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
      <div class="caption">
    
      <p class="brend"><?php print_r($product['manufacturer']); ?></p>
        <h4 class="brend-h4-custom-style"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
        <!-- <p><?php echo $product['description']; ?></p> -->
<!--         <?php if ($product['rating']) { ?>
        <div class="rating">
       
        </div>
        <?php } ?> -->
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
      </div>
      <div class="button-group">
<!--         <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="pr-icon icon-pr-star"></i></button> -->
        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="addto-cart addto-cart-two">В корзину</button>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
  <div class="arrow arrow-left"></div>
  <div class="arrow arrow-right"></div>
      </div>

    
  <script>
  var owl = $(".carousel");
  
  owl.owlCarousel({
  items: 4,
  itemsDesktop: [1199, 3],
  itemsDesktopSmall: [979, 2],
  itemsTablet: [768, 2],
  itemsMobile : [650, 2],
  pagination: false
});
  $(".arrow-right").click(function(){
    owl.trigger("owl.next");
  });
  $(".arrow-left").click(function(){
    owl.trigger("owl.prev");
  });
  
  </script>
     <?php } ?>
</div>


<div class="tab-pane <?php if (!$products) echo'active'?>" id="tab-latest">
    <?php if(isset($watched_data)) { ?>
<div class="cover-carousel2">
  <div class="carousel2">

    <?php foreach ($watched_data as $product) { ?>
    <div class="product-layout">
      <div class="product-thumb transition">
        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
        <div class="caption">
      
        <p class="brend"><?php print_r($product['manufacturer']); ?></p>
          <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
          <!-- <p><?php echo $product['description']; ?></p> -->
  <!--         <?php if ($product['rating']) { ?>
          <div class="rating">
         
          </div>
          <?php } ?> -->
          <?php if ($product['price']) { ?>
          <p class="price">
            <?php if (!$product['special']) { ?>
            <?php echo $product['price']; ?>
            <?php } else { ?>
            <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
            <?php } ?>
           
          </p>
          <?php } ?>
        </div>
        <div class="button-group">
  <!--         <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="pr-icon icon-pr-star"></i></button> -->
          <button type="button" onclick="cart.add('<?php echo $product['id']; ?>');" class="addto-cart addto-cart-two">В корзину</button>
        </div>
      </div>
    </div>
    <?php } ?>

  </div>
    <div class="arrow arrow-left2"></div>
    <div class="arrow arrow-right2"></div>
  </div>





<script type="text/javascript"><!--

var owl2 = $(".carousel2");
  
  owl2.owlCarousel({
  items: 4,
  itemsDesktop: [1199, 3],
  itemsDesktopSmall: [979, 2],
  itemsTablet: [768, 2],
  itemsMobile : [650, 1],
  pagination: false
});
  $(".arrow-right2").click(function(){
    owl.trigger("owl2.next");
  });
  $(".arrow-left2").click(function(){
    owl.trigger("owl2.prev");
  });
--></script>

 

    </div>
</div>
    <?php } ?>
</section>

    <div class="container-fluid">
        <div class="row">
          <section class="news-new news-new-product">
              <div class="col-md-6">
                  <div class="news-new-text">
                      <div class="header">new</div>
                      <p class="header-news">Memorigin Starlit  <br>
                      <span>Legend Tourbillon</span>
                      </p>
                      <a href="#" class="more my-btn">узнать  больше</a>
                  </div>
              </div>            
              <div class="col-md-6">
                  <img src="image/news-new.png" alt="" class="news-image">
              </div>
          </section>
        </div>
    </div>
<?php if($print_image){ ?>
<div class="forprint">
  <img src="<?="image/".$print_image?>" style="display:none" >

</div>
</div>
<?php } ?>
<?php echo $footer; ?>
