﻿<?php echo $header; ?>
<div class="category-contant">
<div class="ccontainer-fluid">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8 col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
<!--       <h2><?php echo $heading_title; ?></h2>
      <?php if ($thumb || $description) { ?>
      <div class="row">
        <?php if ($thumb) { ?>
        <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
        <?php } ?>
        <?php if ($description) { ?>
        <div class="col-sm-10"><?php echo $description; ?></div>
        <?php } ?>
      </div>
      <hr>
      <?php } ?>
      <?php if ($categories) { ?>
      <h3><?php echo $text_refine; ?></h3>
      <?php if (count($categories) <= 5) { ?>
      <div class="row">
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <?php } else { ?>
      <div class="row">
        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?> -->
      <?php if ($products) { ?>
      <div class="row">
        <div class="col-md-6 col-sm-6 hidden-xs">
          <div class="btn-group btn-group-sm">
          
            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>

          </div>
          <div class="filter-sort">
            <span class="sort-title">Сортировать:</span>
            <span class="selected-item"><span>цена: по возрастанию</span> <i></i>
              <ul class="false-select">
                <li class="list-unstyled"><a href=" <?=$sorts['ASC']['href']?>">цена: по возрастанию</a></li>
                  <li class="list-unstyled"><a href=" <?=$sorts['DESC']['href']?>">цена: по убыванию</a></li>
              </ul>
            </span>

          </div>
        </div>
 <!--        <div class="col-md-1 col-sm-6">
          <div class="form-group">
            <a href="<?php echo $compare; ?>" id="compare-total" class="btn btn-link"><?php echo $text_compare; ?></a>
          </div>
        </div> -->
        <!--<div class="col-md-4 col-xs-6">
          <div class="form-group input-group input-group-sm">
            <label class="input-group-addon" for="input-sort"><?php echo $text_sort; ?></label>
            <select id="input-sort" class="form-control" onchange="location = this.value;">
              <?php foreach ($sorts as $sorts) { ?>
              <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
              <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>-->
        <div class="col-md-6 col-xs-12">
          <div class="input-group input-group-sm product-show">
             <label class="" for="input-limit">Товаров на странице:</label>
            <ul id="input-limit" class="" onchange="location = this.value;">
              <?php foreach ($limits as $limits) { ?>
              <?php if ($limits['value'] == $limit) { ?>
              <li selected="selected"><a href="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></a></li>
              <?php } else { ?>
              <li ><a href="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></a></li>
              <?php } ?>
              <?php } ?>
            </ul>
           
          </div>
        </div>
      </div>
      <div class="row row-product-margin-top">
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-list col-xs-12" id="<?=$product['product_id']?>" >
        <div class="eye"></div>
          <div class="product-thumb">
		     <?php if($product['special_discount']){ ?>
                  <span class="discount">
                скидка <span><?=$product['special_discount']?>%</span>
              </span>
			  	<?php }else if($product['new_product']){?>
				<span class="discount">
					Новинка
				</span>

                  <?php } else if($product['date_available']){?>
				    <span class="discount">
                Ожидается с <?=$product['date_available']?>
                  
                </span>
              
				    <?php }?>
		
		
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div>
				<div class="caption">
					<div class="color">
						<div class="color-item">
							<?php foreach($product['parent'] as $color){$parent=strtolower($color['color']);?>
							<input  type="radio" name="color" id="<?=$parent?>"  hidden="" >
							<label old_id="<?=$product['product_id']?>" product_id="<?=$color['product_id']?>" for="<?=$parent?>" class="<?=$parent?>"></label>
							<?php } ?>
						</div>
					</div>
					<p class="brend"><?php print_r($product['manufacturer']); ?></p>
					<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
					<!-- <p><?php echo $product['description']; ?></p> -->
					<!-- <?php if ($product['rating']) { ?>
					<div class="rating">
					  <?php for ($i = 1; $i <= 5; $i++) { ?>
					  <?php if ($product['rating'] < $i) { ?>
					  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } else { ?>
					  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } ?>
					  <?php } ?>
					</div>
					<?php } ?> -->
					<?php if ($product['price']) { ?>
					<p class="price">
					  <?php if (!$product['special']) { ?>
					  <?php echo $product['price']; ?>
					  <?php } else { ?>
					  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
					  <?php } ?>
					  <?php if ($product['tax']) { ?>
					  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
					  <?php } ?>
					</p>
					<?php } ?>
			  </div>
		<div class="button-group">
	<!--         <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="pr-icon icon-pr-star"></i></button> -->
			<button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="addto-cart">В корзину</button>
		  </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-12 text-left pull-right"><?php echo $pagination; ?></div>
       <!--  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div> -->
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</div>

<div class="back back-light">
  
  <div class="preview">

  <div class="exit3"></div>
    <div class="col-md-7">
          <ul class="thumbnails">
                 
            <li class="main-photo"><img src="image/prev1.png" title="" alt="" />
              <span class="discount">
               
              </span>
              <span class="size-up"></span>
              
            </li>
           
    
            <span class="verical-slider">
            
            <li class="image-additional"><img src="image/prev2.png" title="" alt="" /></li>
            <li class="image-additional"><img src="image/prev2.png" title="" alt="" /></li>
            <li class="image-additional"><img src="image/prev2.png" title="" alt="" /></li>
            

            <li class="vertical-tools up"></li>
            <li class="vertical-tools down "></li>
            </span>
            
          </ul> 
           <div class="btn-group">
            <button class="star-button" type="button" data-toggle="tooltip" class="" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="pr2-icon icon-pr2-star"></i></button>
            <button class="share-button"><i class="pr2-icon icon-pr2-share"></i></button>
          </div>
    </div>
    <div class="col-md-5">
            <ul class="list-unstyled">
          <!--   <?php if ($product['manufacturer']) { ?>
            <li><a class="brend" href="<?php echo $manufacturers; ?>"><?php echo $product['manufacturer']; ?></a></li>
            <?php } ?>
            <h1><?php echo $heading_title; ?></h1> -->

            <li><a class="brend" href="">TUDOR HERITAGE </a></li>
           
            <h1>BLACK BAY 36</h1>
<!--             <li><?php echo $text_model; ?> <?php echo $model; ?></li>
            <?php if ($reward) { ?>
            <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
            <?php } ?> -->
            <div class="color">
              <div class="color-title">Цвет:</div>
              <div class="color-item">
                
                <input type="radio" name="color" id="green"  hidden="" >
                <label for="green" class="green"></label>              

                <input type="radio" name="color" id="red"  hidden="">
                <label for="red" class="red"></label>              

                <input type="radio" name="color" id="yellow"  hidden="">
                <label for="yellow" class="yellow"></label>              

                <input type="radio" name="color" id="blue"  hidden="">
                <label for="blue" class="blue"></label>
              </div>
            </div>
            <li class="stock">в наличии</li>
          </ul>

        <?php if ($product['price']) { ?>
          <ul class="list-unstyled price-block">
            <?php if (!$product['special']) { ?>
            <li>
              <h2><?php echo $product['price']; ?></h2>
            </li>
            <?php } else { ?>
            <li><span class="old-price" ><?php echo $product['price']; ?></span></li>
            <li>
              <h2><?php echo $product['special']; ?></h2>
            </li>
            <?php } ?>
          </ul>
           <?php } ?>
               <div class="button-group">
<!--         <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="pr-icon icon-pr-star"></i></button> -->
        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="addto-cart">В корзину</button>
      </div>

              <ul class="nav nav-tabs">
            
            <li class="active"><a href="#tab-specification" data-toggle="tab">Детали</a></li>
            
            <li ><a href="#tab-description" data-toggle="tab">Описание</a></li>
           
            <li><a href="#tab-review" data-toggle="tab">Доставка и оплата</a></li>
        
          </ul>
          <div class="tab-content">
		     <div class="tab-pane active" id="tab-specification">
               <table class="table table-bordered">
               
               </table>
              
              
              </div>
            <div class="tab-pane " id="tab-description">Детали</div>


            <div class="tab-pane" id="tab-review"><?=$category_delivery?></div>
           
          </div>
    </div>
  
  </div>
    <a href="#" class="more">Подробнее ></a>
</div>
</div>
<?php echo $footer; ?>
