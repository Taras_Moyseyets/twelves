<?php echo $header; ?>
</div>

<section class="blog-slider ">
	<div class="container-fluid">
		<div class="row">
			<div class="variable-width">

				<?php foreach($last_news as $news){ ?>
				<div class="slider-opacity">
					<a href="<?=$news['href']?>" >
						<div class="mt-posts-carousel-overlay"></div>
						<div class="text-in-slider">
							<a href="<?=$news['href']?>"><?=$news['title']?></a>
						</div>
						<img src="<?=$news['image']?>" alt="">
					</a>
				</div>
				<?php } ?>

							
			</div>		
		</div>		
	</div>
</section>

<div class="wrapper"> 
<section class="blog-container blog-with-slider">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-8 col-md-8">
  <?php foreach($content as $post) {  ?>
	<div class="blog-items">
			<header>
				<a href="<?=$post['href']?>"><?=$post['title']?></a>
			</header>
			<a class="cover-img-blog" href="<?=$post['href']?>"><img src="<?=$post['image']?>" alt=""></a>
			
			<p><?=$post['text']?> <!--text!--></p>
		
<!-- 		
		<?=$post['time']?>time!
		<br> -->

			<!-- <div class="footer-social">
				<a href="#"><i class="icon-footer icon-facebook"></i></a>
				<a href="#"><i class="icon-footer icon-vk"></i></a>
				<a href="#"><i class="icon-footer icon-odn"></i></a>
				<a href="#"><i class="icon-footer icon-google"></i></a>
				<a href="#"><i class="icon-footer icon-yandex"></i></a>
		    </div> -->
		<a href="<?=$post['href']?>" class="all-article">читать дальше</a>
	</div>
  
  <?php } ?>
  				
			</div>
  			<div class="col-sm-4 col-md-4 snews">
				<div class="snews-item">
					<a href="<?=$banner[0]['href']?>"><img src="image/<?=$banner[0]['image']?>" alt=""></a>
					<!--<header>Для тех, над кем <p>не властно время.</p> </header>!
					<a href="#" class="more my-btn">узнать  больше</a>-->
				</div>
				<div class="snews-item other-snews">
					<a href="<?=$banner[1]['href']?>"><img src="image/<?=$banner[1]['image']?>" alt=""></a>
					<!--<header>КАЧЕСТВО ДИЗАЙН СТИЛЬ</header>!
					<a href="#" class="more my-btn">узнать  больше</a>-->
				</div>
				<div class="snews-item payed">
						<img src="image/snews3.jpg" alt="">
					<header>Reveals Arena Metasonic Watch <span>Gerald Genta</span></header>
					<div class="discount">скидка <span>40%</span></div>
					<a href="#" class="more my-btn">Купить</a>
				</div>
			</div>
		</div>

	</div>
</section>

<div class="container">
	<div class="row">
        <div class="col-sm-8 text-center"><?php echo $pagination; ?></div>
        <div class="col-sm-4 text-center"><!-- <?php echo $results; ?> --></div>
    </div>
</div>

 <?php echo $footer; ?>