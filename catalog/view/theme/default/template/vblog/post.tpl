<?php echo $header; ?>

<section class="blog-container blog-post">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-8 col-md-8">
            	<div class="blog-items">
            			<header>
            				<?=$post['title'];?><!--name!-->
            				
            			</header>
            			<div class="date"><?=$post['time'];?></div>
            				<!--time!-->
            			<img src="<?=$post['image']?>" >

            			<p><?=$post['text']?> <!--text!--></p>
            		
            <!-- 		
            		<?=$post['time']?>time!
            		<br> -->
            
            			<!--
                        <div class="footer-social">
            				<a href="#"><i class="icon-footer icon-facebook"></i></a>
            				<a href="#"><i class="icon-footer icon-vk"></i></a>
            				<a href="#"><i class="icon-footer icon-odn"></i></a>
            				<a href="#"><i class="icon-footer icon-google"></i></a>
            				<a href="#"><i class="icon-footer icon-yandex"></i></a>
            		    </div>
          		        -->
            	</div>
			</div>
  			<div class="col-sm-4 col-md-4 snews">
				<div class="snews-item">
					<div class="blog-items">
                        <a class="cover-img-blog" href="<?=$last_news['href']?>"><img src="<?=$last_news['image']?>" alt=""></a>
                        
                        <p><?=$last_news['text']?> <!--text!--></p>
                       
                    </div>
				</div>
				<div class="snews-item other-snews">
					<div class="instra">
				        <div class="row my-row"> 
				           <?php for($i=0;$i<2;$i++){?>
				             <a href="<?=$instagram[$i]['link']?>" target="_blank" class="mass-class">
				                <img src="<?=$instagram[$i]['image']?>" alt="" class="instra-link">
				            </a>
				            <?php } ?>
				        </div>
				        <div class="row my-row"> 
				              <?php for($i=2;$i<4;$i++){?>
				            <a href="<?=$instagram[$i]['link']?>" target="_blank" class="mass-class">
				                <img src="<?=$instagram[$i]['image']?>" alt="" class="instra-link">
				            </a>
				            <?php } ?>
				        </div>
					</div>
				</div>
				<div class="snews-item payed">
					<img src="image/snews3.jpg" alt="">
					<header>Reveals Arena Metasonic Watch <span>Gerald Genta</span></header>
					<div class="discount">скидка <span>40%</span></div>
					<a href="#" class="more my-btn">Купить</a>
				</div>
			</div>
		</div>
	</div>
</section>

<?php echo $footer; ?>